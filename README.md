# SMEV
SOAP Message Essential Validation

## Описание модулей
Детальное описание модуля можно найти по ссылке.
1. [fias_stub](fias_stub) - заглушка SOAP сервиса ФИАС
2. [helm](helm) - helm чарты и переменные для запуска демо
3. [postman](postman) - коллекция запросов для верификации решения
4. [smev_core](smev_core) - ядро логики валидации
5. [smev_tyk](smev_tyk) - плагин валидации для шлюза Tyk
6. [smev_cli](smev_cli) - CLI утилита для разработки и внедрения SOAP API
7. [tyk](tyk) - докер образ Tyk, в который добавлен плагин

## Сборка проекта
### Пререквизиты
1. go 1.15+
2. docker

### Сборка
выполнить команду build:
```bash
make build
```

## Запуск шлюза (локальная разработка)
Перед запуском обязательно выполните сборку
```bash
make start
```

## Публикация артефактов
Для публикации необходим доступ к проекту harbor.bankingapi.ru/team25 .

Выполнить команду push:
```bash
make push
```
Артефакт smev_cli/smev пока что не публикуется на удаленный сервер.


## Установка демо окружения
## Пререквизиты
1. kubernetes & kubectl
2. helm
3. сборка проекта и публикация артефактов

## Установка
выполнить команду deploy:
```bash
make deploy
```
После завершения установки прочитать k8s секрет с паролем пользователя default@example.com:
```build
make password
```
с этой учетной записью можно зайти в панель управления Tyk

## Демо ресурсы
1. Панель управления: [team25.bankingapi.ru](http://team25.bankingapi.ru)
2. Шлюз API: [gw.team25.bankingapi.ru](http://gw.team25.bankingapi.ru)
3. План демо: [DEMO.md](DEMO.md)