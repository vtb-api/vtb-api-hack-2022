## smev apidef

генерация tyk api definition из WSDL

```
smev apidef [flags]
```

### Options

```
      --allow-undeclared-body-attributes       разрешить произвольные атрибуты в Envelope.Body
      --allow-undeclared-envelope-attributes   разрешить произвольные атрибуты в Envelope
      --allow-undeclared-envelope-elements     разрешить произвольные элементы в Envelope
      --allow-undeclared-faults                разрешить ошибки в стандартном формате (default true)
      --allow-undeclared-header-attributes     разрешить произвольные атрибуты в Envelope.Header
      --allow-undeclared-header-elements       разрешить произвольные элементы в Envelope.Header
  -h, --help                                   help for apidef
  -l, --listen-path string                     путь, по которому будет доступно API на шлюзе
  -p, --target-port string                     Service Port name для импорта(Обязателен при наличии нескольких Port)
  -s, --target-service string                  Service name для импорта(Обязателен при наличии нескольких сервисов)
  -u, --upstream string                        url, по которому будет доступен backend API
      --validate-response                      валидировать ответы backend
  -w, --wsdl string                            путь к wsdl
```

### SEE ALSO

* [smev](smev.md)	 - smev это мультитул для разработки SOAP сервисов

###### Auto generated by spf13/cobra on 15-Oct-2022
