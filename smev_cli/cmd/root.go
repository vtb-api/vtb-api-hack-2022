package cmd

import (
	"github.com/spf13/cobra"
	"os"
)

func init() {

}

var RootCmd = &cobra.Command{
	Use:   "smev",
	Short: "smev это мультитул для разработки SOAP сервисов",
	Long: `smev позволяет валидировать WSDL, SOAP сообщения, генерировать api для Tyk,
просматривать схемы валидации SOAP сообщений`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			_ = cmd.Help()
			os.Exit(0)
		}
	},
}
