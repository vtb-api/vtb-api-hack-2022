package cmd

import (
	"fmt"
	"github.com/lestrrat-go/libxml2/xsd"
	"github.com/pkg/errors"
	"github.com/rshlin/smev_cli/common"
	"github.com/rshlin/smev_cli/options"
	"github.com/rshlin/smev_cli/validate"
	"github.com/rshlin/smev_core/soap"
	"github.com/spf13/cobra"
	"os"
	"strings"
)

var validateCmd = &cobra.Command{
	Use:   "validate",
	Short: "валидация SOAP сообщения",
	Run: func(cmd *cobra.Command, args []string) {
		rq, err := newValidateRqFromCmd(cmd)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		err = validate.SoapEnvelope(rq)
		if err != nil {
			msg := sprintValidationErrors(err)
			fmt.Println(msg)
			os.Exit(1)
		}
		fmt.Println("message is valid")
	},
}

func newValidateRqFromCmd(cmd *cobra.Command) (*validate.Request, error) {
	wsdlOptions, err := options.GetWsdlOptions(cmd)
	if err != nil {
		return nil, err
	}
	soapOptions, err := options.GetSoapOptions(cmd)
	if err != nil {
		return nil, err
	}
	wsdlPath, err := options.GetWsdlPath(cmd)
	if err != nil {
		return nil, err
	}
	envelopePath, err := cmd.Flags().GetString("message-path")
	if err != nil {
		return nil, err
	}
	soapAction, err := cmd.Flags().GetString("soap-action")
	if err != nil {
		return nil, err
	}
	if soapAction == "" {
		return nil, fmt.Errorf("необходимо указать soap-action")
	}
	rq, err := cmd.Flags().GetBool("rq")
	if err != nil {
		return nil, err
	}
	rs, err := cmd.Flags().GetBool("rs")
	if err != nil {
		return nil, err
	}
	if rs == rq {
		return nil, fmt.Errorf("небходимо выбрать валидацию либо запроса, либо ответа")
	}
	request := validate.Request{
		ValidateRq:   rq,
		WsdlPath:     wsdlPath,
		EnvelopePath: envelopePath,
		WsdlOptions:  wsdlOptions,
		SoapOptions:  soapOptions,
		SoapAction:   soap.Action(soapAction),
	}
	return &request, nil
}

func sprintValidationErrors(err error) string {
	messages := []string{errors.WithStack(err).Error()}
	if sve, ok := err.(xsd.SchemaValidationError); ok {
		messages = append(messages, "============Validation Errors================")
		for _, e := range sve.Errors() {
			messages = append(messages, e.Error())
		}
		messages = append(messages, "=============================================")
	}
	return strings.Join(messages, "\n")
}

func init() {
	RootCmd.AddCommand(validateCmd)

	validateCmd.Flags().Bool("rq", true, "валидация запроса")
	validateCmd.Flags().Bool("rs", false, "валидация ответа")
	validateCmd.Flags().StringP("soap-action", "a", "", "soap action SOAP операции")
	validateCmd.Flags().StringP("message-path", "m", "", "путь к xml с SOAP конвертом")
	common.AddWsdlFlags(validateCmd)
	common.AddSoapFlags(validateCmd)
}
