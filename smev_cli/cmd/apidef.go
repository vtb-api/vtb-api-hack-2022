package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/rshlin/smev_cli/apidef"
	"github.com/rshlin/smev_cli/apidef/objects"
	"github.com/rshlin/smev_cli/common"
	"github.com/rshlin/smev_cli/options"
	"github.com/spf13/cobra"
	"os"
)

var apidefCmd = &cobra.Command{
	Use:   "apidef",
	Short: "генерация tyk api definition из WSDL",
	Run: func(cmd *cobra.Command, args []string) {
		rq, err := newGenerateApidefRqFromCmd(cmd)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		def, err := apidef.GenerateApiDef(rq)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		raw, err := marshalResult(def)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		_, _ = os.Stdout.Write(raw)
	},
}

func marshalResult(def *objects.APIDefinition) ([]byte, error) {
	var buf bytes.Buffer
	encoder := json.NewEncoder(&buf)
	encoder.SetEscapeHTML(false)
	encoder.SetIndent("", "  ")
	if err := encoder.Encode(def); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func newGenerateApidefRqFromCmd(cmd *cobra.Command) (*apidef.GenerateRq, error) {
	wsdlOptions, err := options.GetWsdlOptions(cmd)
	if err != nil {
		return nil, err
	}
	soapOptions, err := options.GetSoapOptions(cmd)
	if err != nil {
		return nil, err
	}
	wsdlPath, err := options.GetWsdlPath(cmd)
	if err != nil {
		return nil, err
	}
	listenPath, err := cmd.Flags().GetString("listen-path")
	if err != nil {
		return nil, err
	}
	upstream, err := cmd.Flags().GetString("upstream")
	if err != nil {
		return nil, err
	}
	validateRs, err := cmd.Flags().GetBool("validate-response")
	if err != nil {
		return nil, err
	}
	rq := apidef.GenerateRq{
		ListenPath:  listenPath,
		UpstreamUrl: upstream,
		ValidateRs:  validateRs,
		WsdlPath:    wsdlPath,
		WsdlOptions: wsdlOptions,
		SoapOptions: soapOptions,
	}
	return &rq, nil
}

func init() {
	RootCmd.AddCommand(apidefCmd)

	apidefCmd.Flags().StringP("listen-path", "l", "", "путь, по которому будет доступно API на шлюзе")
	apidefCmd.Flags().StringP("upstream", "u", "", "url, по которому будет доступен backend API")
	apidefCmd.Flags().Bool("validate-response", false, "валидировать ответы backend")
	common.AddWsdlFlags(apidefCmd)
	common.AddSoapFlags(apidefCmd)
}
