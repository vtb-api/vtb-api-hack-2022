package cmd

import (
	"fmt"
	"github.com/rshlin/smev_cli/common"
	"github.com/rshlin/smev_cli/options"
	"github.com/rshlin/smev_cli/validate"
	"github.com/rshlin/smev_core/soap"
	"github.com/spf13/cobra"
	"os"
)

var inspectCmd = &cobra.Command{
	Use:   "inspect",
	Short: "генерация схемы валидатора SOAP операции",
	Long:  "генерация схемы валидатора SOAP операции. служит для анализа результата валидатора",
	Run: func(cmd *cobra.Command, args []string) {
		rq, err := newInspectRqFromCmd(cmd)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		schemaRaw, err := validate.InspectOperationSchema(rq)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		_, _ = os.Stdout.Write([]byte(schemaRaw))
	},
}

func newInspectRqFromCmd(cmd *cobra.Command) (*validate.Request, error) {
	wsdlOptions, err := options.GetWsdlOptions(cmd)
	if err != nil {
		return nil, err
	}
	soapOptions, err := options.GetSoapOptions(cmd)
	if err != nil {
		return nil, err
	}
	wsdlPath, err := options.GetWsdlPath(cmd)
	if err != nil {
		return nil, err
	}
	soapAction, err := cmd.Flags().GetString("soap-action")
	if err != nil {
		return nil, err
	}
	if soapAction == "" {
		return nil, fmt.Errorf("необходимо указать soap-action")
	}
	rq, err := cmd.Flags().GetBool("rq")
	if err != nil {
		return nil, err
	}
	rs, err := cmd.Flags().GetBool("rs")
	if err != nil {
		return nil, err
	}
	if rs == rq {
		return nil, fmt.Errorf("небходимо выбрать валидацию либо запроса, либо ответа")
	}
	request := validate.Request{
		ValidateRq:  rq,
		WsdlPath:    wsdlPath,
		WsdlOptions: wsdlOptions,
		SoapOptions: soapOptions,
		SoapAction:  soap.Action(soapAction),
	}
	return &request, nil
}

func init() {
	RootCmd.AddCommand(inspectCmd)

	inspectCmd.Flags().Bool("rq", true, "валидация запроса")
	inspectCmd.Flags().Bool("rs", false, "валидация ответа")
	inspectCmd.Flags().StringP("soap-action", "a", "", "soap action SOAP операции")
	common.AddWsdlFlags(inspectCmd)
	common.AddSoapFlags(inspectCmd)
}
