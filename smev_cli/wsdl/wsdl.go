package wsdl

import (
	"github.com/lestrrat-go/libxml2"
	"github.com/lestrrat-go/libxml2/types"
	"github.com/lestrrat-go/libxml2/xpath"
	"github.com/rshlin/smev_core/wsdl"
	xpath2 "github.com/rshlin/smev_core/xpath"
	"io/ioutil"
	"path/filepath"
)

type SchemaFile struct {
	Content string `json:"content"`
	Name    string `json:"name"`
}

type File struct {
	Wsdl     *wsdl.WSDL
	WsdlFile SchemaFile
	Schemas  []SchemaFile
}

func GetFromFile(wsdlPath string, options *wsdl.Options) (*File, error) {
	wsdl11, err := wsdl.NewWsdlFromFile(wsdlPath, options)
	if err != nil {
		return nil, err
	}
	schemas, err := getSchemas(wsdl11)
	if err != nil {
		return nil, err
	}
	f := File{
		Wsdl:     wsdl11,
		WsdlFile: schemaFile(wsdl11),
		Schemas:  schemas,
	}
	return &f, nil
}

func getSchemas(wsdl11 *wsdl.WSDL) ([]SchemaFile, error) {
	wsdlDir := filepath.Dir(wsdl11.WsdlUrl)
	schemaByName := make(map[string]types.Node)
	for _, schema := range wsdl11.RootSchemas {
		schema.SchemaLocation = filepath.Join(wsdlDir, schema.SchemaLocation)
		name := filepath.Base(schema.SchemaLocation)
		s := schemaWithUpdatedLoc{
			Schema:                *schema,
			UpdatedSchemaLocation: name,
		}
		if err := locateImportsAndIncludes(&s, schemaByName); err != nil {
			return nil, err
		}
	}

	var files []SchemaFile
	for name, content := range schemaByName {
		files = append(files, SchemaFile{
			Content: content.String(),
			Name:    name,
		})
	}
	return files, nil
}

type schemaWithUpdatedLoc struct {
	wsdl.Schema
	UpdatedSchemaLocation string
}

func locateImportsAndIncludes(schema *schemaWithUpdatedLoc, schemaByName map[string]types.Node) error {
	// 1. include current schema
	schemaByName[schema.UpdatedSchemaLocation] = schema.Doc
	processSchema := func(s types.Node) error {
		e := s.(types.Element)
		schemaLocation, err := xpath2.AttrValue(s, "schemaLocation")
		if err != nil {
			return err
		}
		if !filepath.IsAbs(schemaLocation) {
			parentSchemaDir := filepath.Dir(schema.SchemaLocation)
			schemaLocation = filepath.Join(parentSchemaDir, schemaLocation)
		}
		filename := filepath.Base(schemaLocation)
		// 3. locate their schemaLocation and change it
		err = e.SetAttribute("schemaLocation", filename)
		if err != nil {
			return err
		}
		ns := e.NamespaceURI()
		doc, err := fetchSchema(schemaLocation)
		if err != nil {
			return err
		}
		// 4. build schema obj and invoke this func
		schema := schemaWithUpdatedLoc{
			Schema: wsdl.Schema{
				Namespace:      ns,
				SchemaLocation: schemaLocation,
				Doc:            doc,
			},
			UpdatedSchemaLocation: filename,
		}
		return locateImportsAndIncludes(&schema, schemaByName)
	}
	// 2. xpath all includes and imports
	imports := xpath.NodeList(schema.Doc.Find(`.//*[local-name()="import" and namespace-uri()="http://www.w3.org/2001/XMLSchema"]`))
	for _, node := range imports {
		if err := processSchema(node); err != nil {
			return err
		}
	}
	includes := xpath.NodeList(schema.Doc.Find(`.//*[local-name()="include" and namespace-uri()="http://www.w3.org/2001/XMLSchema"]`))
	for _, node := range includes {
		if err := processSchema(node); err != nil {
			return err
		}
	}

	return nil
}

func fetchSchema(location string) (types.Node, error) {
	file, err := ioutil.ReadFile(location)
	if err != nil {
		return nil, err
	}
	doc, err := libxml2.Parse(file)
	if err != nil {
		return nil, err
	}

	return doc, nil
}

func schemaFile(w *wsdl.WSDL) SchemaFile {
	name := filepath.Base(w.WsdlUrl)
	content := w.Doc.String()
	return SchemaFile{
		Content: content,
		Name:    name,
	}
}
