package apidef

import (
	"github.com/TykTechnologies/tyk/apidef"
	"github.com/gosimple/slug"
	"github.com/rshlin/smev_cli/apidef/objects"
	wsdlFile "github.com/rshlin/smev_cli/wsdl"
	"github.com/rshlin/smev_core/soap"
	"github.com/rshlin/smev_core/wsdl"
	uuid "github.com/satori/go.uuid"
)

const defaultVersion = "Default"
const defaultExpires = "3000-01-02 15:04"
const tagHeaderSoapAction = "SOAPAction"

type GenerateRq struct {
	ListenPath  string
	UpstreamUrl string
	WsdlPath    string
	ValidateRs  bool
	WsdlOptions *wsdl.Options
	SoapOptions *soap.Options
}

type smevConfigData struct {
	Schemas     []wsdlFile.SchemaFile `json:"schemas"`
	SoapOptions soap.Options          `json:"soap_options"`
	Wsdl        wsdlFile.SchemaFile   `json:"wsdl"`
	WsdlOptions wsdl.Options          `json:"wsdl_options"`
}

func GenerateApiDef(rq *GenerateRq) (*objects.APIDefinition, error) {
	file, err := wsdlFile.GetFromFile(rq.WsdlPath, rq.WsdlOptions)
	if err != nil {
		return nil, err
	}
	ad := newBlankDBDashDefinition()
	ad.Name = file.Wsdl.TargetService.Name
	ad.Slug = slug.Make(file.Wsdl.TargetService.Name)
	ad.Active = true
	ad.UseKeylessAccess = true
	ad.APIID = uuid.NewV4().String()

	ad.VersionData.NotVersioned = true
	ad.VersionData.Versions = make(map[string]apidef.VersionInfo)
	ad.VersionData.Versions[defaultVersion] = newEmptyVersion()

	ad.Proxy.ListenPath = rq.ListenPath
	ad.Proxy.StripListenPath = true
	ad.Proxy.TargetURL = rq.UpstreamUrl

	ad.CustomMiddleware = genMiddlewareConfig(rq)

	ad.ConfigData = genConfigData(rq, file)

	ad.TagHeaders = make([]string, 1)
	ad.TagHeaders[0] = tagHeaderSoapAction

	return ad.APIDefinition, nil
}

func genMiddlewareConfig(rq *GenerateRq) apidef.MiddlewareSection {
	post := make([]apidef.MiddlewareDefinition, 1)
	post[0] = apidef.MiddlewareDefinition{
		Name:           "ValidateSoapRequest",
		Path:           "/opt/tyk-gateway/middleware/smev_tyk.so",
		RequireSession: false,
		RawBodyOnly:    false,
	}
	var response []apidef.MiddlewareDefinition
	if rq.ValidateRs {
		response = make([]apidef.MiddlewareDefinition, 1)
		response[0] = apidef.MiddlewareDefinition{
			Name:           "ValidateSoapResponse",
			Path:           "/opt/tyk-gateway/middleware/smev_tyk.so",
			RequireSession: false,
			RawBodyOnly:    false,
		}
	}
	return apidef.MiddlewareSection{
		Pre:         nil,
		Post:        post,
		PostKeyAuth: nil,
		AuthCheck:   apidef.MiddlewareDefinition{},
		Response:    response,
		Driver:      "goplugin",
		IdExtractor: apidef.MiddlewareIdExtractor{},
	}
}

func genConfigData(rq *GenerateRq, file *wsdlFile.File) map[string]interface{} {
	configData := make(map[string]interface{}, 1)
	configData["smev"] = smevConfigData{
		Schemas:     file.Schemas,
		SoapOptions: *rq.SoapOptions,
		Wsdl:        file.WsdlFile,
		WsdlOptions: *rq.WsdlOptions,
	}

	return configData
}

func newBlankDBDashDefinition() *objects.DBApiDefinition {
	EmptyMW := apidef.MiddlewareSection{
		Pre:  make([]apidef.MiddlewareDefinition, 0),
		Post: make([]apidef.MiddlewareDefinition, 0),
	}

	def := &objects.APIDefinition{}
	def.ConfigData = map[string]interface{}{}
	def.ResponseProcessors = make([]apidef.ResponseProcessor, 0)
	def.AllowedIPs = make([]string, 0)
	def.CustomMiddleware = EmptyMW
	def.Tags = make([]string, 0)
	return &objects.DBApiDefinition{
		APIDefinition: def,
	}
}
func newEmptyVersion() apidef.VersionInfo {
	// We need this because sometimes the schemas reject null values
	d := apidef.VersionInfo{}
	d.Name = defaultVersion
	d.Expires = defaultExpires
	d.Paths.BlackList = make([]string, 0)
	d.Paths.WhiteList = make([]string, 0)
	d.Paths.Ignored = make([]string, 0)

	return d
}
