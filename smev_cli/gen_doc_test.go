//go:build gendoc

package main

import (
	"github.com/rshlin/smev_cli/cmd"
	"github.com/spf13/cobra/doc"
	"testing"
)

func TestGenDoc(t *testing.T) {
	_ = doc.GenMarkdownTree(cmd.RootCmd, "doc")
}
