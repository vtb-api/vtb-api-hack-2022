package common

import "github.com/spf13/cobra"

func AddWsdlFlags(cmd *cobra.Command) {
	cmd.Flags().StringP("wsdl", "w", "", "путь к wsdl")
	cmd.Flags().StringP("target-service", "s", "", "Service name для импорта(Обязателен при наличии нескольких сервисов)")
	cmd.Flags().StringP("target-port", "p", "", "Service Port name для импорта(Обязателен при наличии нескольких Port)")
}

func AddSoapFlags(cmd *cobra.Command) {
	cmd.Flags().Bool("allow-undeclared-envelope-elements", false, "разрешить произвольные элементы в Envelope")
	cmd.Flags().Bool("allow-undeclared-envelope-attributes", false, "разрешить произвольные атрибуты в Envelope")
	cmd.Flags().Bool("allow-undeclared-header-elements", false, "разрешить произвольные элементы в Envelope.Header")
	cmd.Flags().Bool("allow-undeclared-header-attributes", false, "разрешить произвольные атрибуты в Envelope.Header")
	cmd.Flags().Bool("allow-undeclared-faults", true, "разрешить ошибки в стандартном формате")
	cmd.Flags().Bool("allow-undeclared-body-attributes", false, "разрешить произвольные атрибуты в Envelope.Body")
}
