package validate

import (
	"fmt"
	"github.com/lestrrat-go/libxml2"
	wsdl2 "github.com/rshlin/smev_cli/wsdl"
	"github.com/rshlin/smev_core/soap"
	"github.com/rshlin/smev_core/wsdl"
	"io/ioutil"
)

type Request struct {
	WsdlPath     string
	EnvelopePath string
	WsdlOptions  *wsdl.Options
	SoapOptions  *soap.Options
	ValidateRq   bool
	SoapAction   soap.Action
}

func SoapEnvelope(rq *Request) error {
	validator, err := getValidator(rq)
	if err != nil {
		return err
	}
	raw, err := ioutil.ReadFile(rq.EnvelopePath)
	if err != nil {
		return err
	}
	doc, err := libxml2.Parse(raw)
	if err != nil {
		return err
	}
	if rq.ValidateRq {
		return validator.ValidateRq(doc, rq.SoapAction)
	}

	return validator.ValidateRs(doc, rq.SoapAction)
}

func getValidator(rq *Request) (soap.Validator, error) {
	file, err := wsdl2.GetFromFile(rq.WsdlPath, rq.WsdlOptions)
	if err != nil {
		return nil, err
	}
	validator, err := soap.NewSoapValidator(file.Wsdl, rq.SoapOptions)
	if err != nil {
		return nil, err
	}
	return validator, nil
}

func InspectOperationSchema(rq *Request) (string, error) {
	validator, err := getValidator(rq)
	if err != nil {
		return "", err
	}
	if v11, ok := interface{}(validator).(*soap.V11Validator); ok {
		operationValidator, ok := v11.Operations[rq.SoapAction]
		if !ok {
			return "", fmt.Errorf("couldn't locate soap action [%v]", rq.SoapAction)
		}
		if rq.ValidateRq {
			return operationValidator.RequestSchemaDoc.String(), nil
		}
		return operationValidator.ResponseSchemaDoc.String(), nil
	}
	return "", fmt.Errorf("unsupported validator implementation")
}
