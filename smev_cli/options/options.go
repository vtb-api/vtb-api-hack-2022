package options

import (
	"fmt"
	"github.com/rshlin/smev_core/soap"
	"github.com/rshlin/smev_core/wsdl"
	"github.com/spf13/cobra"
)

func GetWsdlOptions(cmd *cobra.Command) (*wsdl.Options, error) {
	_, err := GetWsdlPath(cmd)
	if err != nil {
		return nil, err
	}
	targetService, err := cmd.Flags().GetString("target-service")
	if err != nil {
		return nil, err
	}
	targetPort, err := cmd.Flags().GetString("target-port")
	if err != nil {
		return nil, err
	}

	opts := wsdl.Options{
		TargetService: targetService,
		TargetPort:    targetPort,
	}
	return &opts, nil
}

func GetSoapOptions(cmd *cobra.Command) (*soap.Options, error) {
	_, err := GetWsdlPath(cmd)
	if err != nil {
		return nil, err
	}
	allowEnvelopeElements, err := cmd.Flags().GetBool("allow-undeclared-envelope-elements")
	if err != nil {
		return nil, err
	}
	allowEnvelopeAttributes, err := cmd.Flags().GetBool("allow-undeclared-envelope-attributes")
	if err != nil {
		return nil, err
	}
	allowHeaderElements, err := cmd.Flags().GetBool("allow-undeclared-header-elements")
	if err != nil {
		return nil, err
	}
	allowHeaderAttributes, err := cmd.Flags().GetBool("allow-undeclared-header-attributes")
	if err != nil {
		return nil, err
	}
	allowFaults, err := cmd.Flags().GetBool("allow-undeclared-faults")
	if err != nil {
		return nil, err
	}
	allowBodyAttributes, err := cmd.Flags().GetBool("allow-undeclared-body-attributes")
	if err != nil {
		return nil, err
	}

	opts := soap.Options{
		AllowUndeclaredEnvelopeElements:   allowEnvelopeElements,
		AllowUndeclaredEnvelopeAttributes: allowEnvelopeAttributes,
		AllowUndeclaredHeaderElements:     allowHeaderElements,
		AllowUndeclaredHeaderAttributes:   allowHeaderAttributes,
		AllowUndeclaredFaults:             allowFaults,
		AllowUndeclaredBodyAttributes:     allowBodyAttributes,
	}
	return &opts, nil
}

func GetWsdlPath(cmd *cobra.Command) (string, error) {
	wsdlPath, err := cmd.Flags().GetString("wsdl")
	if err != nil {
		return "", err
	}
	if wsdlPath == "" {
		return "", fmt.Errorf("нужно указать путь к файлу wsdl")
	}
	return wsdlPath, nil
}
