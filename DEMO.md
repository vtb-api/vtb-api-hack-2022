# План Демо
План подготовлен для оценки реализации функциональных требований:
1. Проверка запросов и ответов XML с несколькими namespace (по ФЛК)
2. Возможность экспорта/импорта Wsdl и Xsd для применения проверок 
3. Возможность использования инструмента локально, без обмена (Postman или Soap UI). 
4. Возможность просмотра результатов проверки, с расшифровкой по: участникам потока, наименованию API, endpoint’ам и др. 
5. Возможность настройки метрик мониторинга, сбора статистики 
6. Заявлена аутентификация через Open ID connect;
## 1. Проверка запросов и ответов XML с несколькими namespace (по ФЛК)
Импортировать [postman коллекцию](postman) и активировать в интерфейсе демо окружение
Выполнить следующие запросы и проверить:
1. запрос адреса

   Проверить:
    * получен ответ **getStrictAddressZaprosResponse**
    * запрос содержал в заголовке OIDC Access Token (в логах постман). показать содержимое с jwt.io
    * Access Token был выписан сервером **https://auth.bankingapi.ru/auth/realms/kubernetes** для clientId **team25**
2. вызвать API без Access Token

   Проверить:
   * Отсутствие заголовка Authorization в запросе
   * Был получен ответ
   ```xml
   <?xml version = '1.0' encoding = 'UTF-8'?>
   <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
     <SOAP-ENV:Body>
       <SOAP-ENV:Fault>
         <faultcode xsi:type = "xsd:string">Gateway error</faultcode>
         <faultstring xsi:type = "xsd:string">
           Authorization field missing
         </faultstring>
       </SOAP-ENV:Fault>
     </SOAP-ENV:Body>
   </SOAP-ENV:Envelope>
   ```
3. исключить обязательный атрибут Envelope.body.getStrictAddressZapros.MessageData.AppData.Документ#ИдДок
   
   Проверить:
   * Получен ответ с статус кодом 400 с ошибкой валидации
   ```xml
   <?xml version = '1.0' encoding = 'UTF-8'?>
    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
       <SOAP-ENV:Body>
         <SOAP-ENV:Fault>
           <faultcode xsi:type = "xsd:string">400</faultcode>
           <faultstring xsi:type = "xsd:string">
             schema validation failed
   ============Validation Errors================
   Element '{http://ws.unisoft/getAddressZapros}Документ': The attribute 'ИдДок' is required but missing.
   =============================================
           </faultstring>
         </SOAP-ENV:Fault>
       </SOAP-ENV:Body>
   </SOAP-ENV:Envelope>
   ``` 
4. исключить обязательный элемент Envelope.body.getStrictAddressZapros.MessageData.AppData.Документ.ЗапросАдрес
   
   Проверить:
   * Получен ответ с статус кодом 400 с ошибкой валидации
   ```xml
   <?xml version = '1.0' encoding = 'UTF-8'?>
    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
       <SOAP-ENV:Body>
         <SOAP-ENV:Fault>
           <faultcode xsi:type = "xsd:string">400</faultcode>
           <faultstring xsi:type = "xsd:string">
             schema validation failed
   ============Validation Errors================
   Element '{http://ws.unisoft/getAddressZapros}Документ': Missing child element(s). Expected is ( {http://ws.unisoft/getAddressZapros}ЗапросАдрес ).
   =============================================
           </faultstring>
         </SOAP-ENV:Fault>
       </SOAP-ENV:Body>
   </SOAP-ENV:Envelope>
   ```
5. ответ с кекорректным названием елемента Envelope.Body.Hello

   Проверить:
   * Получен ответ с статус кодом 501 с ошибкой валидации
   ```xml
   <?xml version = '1.0' encoding = 'UTF-8'?>
    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
       <SOAP-ENV:Body>
         <SOAP-ENV:Fault>
           <faultcode xsi:type = "xsd:string">400</faultcode>
           <faultstring xsi:type = "xsd:string">
             schema validation failed
   ============Validation Errors================
   Element '{http://ws.unisoft/}Hello': This element is not expected. Expected is ( {http://ws.unisoft/}getStrictAddressZaprosResponse ).
   =============================================
           </faultstring>
         </SOAP-ENV:Fault>
       </SOAP-ENV:Body>
   </SOAP-ENV:Envelope>
   ```   
6. ответ с некорректным атрибутом(длина) Envelope.Body.getStrictAddressZaprosResponse.MessageData.AppData.Адрес.ИдФиас

   Проверить:
   * Получен ответ с статус кодом 501 с ошибкой валидации
   ```xml
   <?xml version = '1.0' encoding = 'UTF-8'?>
    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
       <SOAP-ENV:Body>
         <SOAP-ENV:Fault>
           <faultcode xsi:type = "xsd:string">400</faultcode>
           <faultstring xsi:type = "xsd:string">
             schema validation failed
   ============Validation Errors================
   Element '{http://ws.unisoft/getAddressZaprosResponse}ИдФиас': [facet 'length'] The value has a length of '1'; this differs from the allowed length of '36'.
   =============================================
           </faultstring>
         </SOAP-ENV:Fault>
       </SOAP-ENV:Body>
   </SOAP-ENV:Envelope>
   ```   
7. ответ с отсутствующим атрибутом Envelope.Body.getStrictAddressZaprosResponse.MessageData.AppData.Адрес.ИдЗапрос

   Проверить:
   * Получен ответ с статус кодом 501 с ошибкой валидации
   ```xml
   <?xml version = '1.0' encoding = 'UTF-8'?>
    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
       <SOAP-ENV:Body>
         <SOAP-ENV:Fault>
           <faultcode xsi:type = "xsd:string">400</faultcode>
           <faultstring xsi:type = "xsd:string">
             schema validation failed
   ============Validation Errors================
   Element '{http://ws.unisoft/getAddressZaprosResponse}Адрес': The attribute 'ИдЗапрос' is required but missing.
   =============================================
           </faultstring>
         </SOAP-ENV:Fault>
       </SOAP-ENV:Body>
   </SOAP-ENV:Envelope>
   ```   
 8. запрос c неизвестным заголовком Envelope.Header.Security

   Проверить:
   * Получен ответ с статус кодом 400 с ошибкой валидации
   ```xml
   <?xml version = '1.0' encoding = 'UTF-8'?>
    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
       <SOAP-ENV:Body>
         <SOAP-ENV:Fault>
           <faultcode xsi:type = "xsd:string">400</faultcode>
           <faultstring xsi:type = "xsd:string">
             schema validation failed
   ============Validation Errors================
   Element '{http://schemas.xmlsoap.org/soap/envelope/}Header': Character content is not allowed, because the content type is empty.
   Element '{http://schemas.xmlsoap.org/soap/envelope/}Header': Element content is not allowed, because the content type is empty.
   =============================================
           </faultstring>
         </SOAP-ENV:Fault>
       </SOAP-ENV:Body>
   </SOAP-ENV:Envelope>
   ```   
9. запрос c неизвестным атрибутом Envelope.Body#wsu:Id

   Проверить:
   * Получен ответ с статус кодом 400 с ошибкой валидации
   ```xml
   <?xml version = '1.0' encoding = 'UTF-8'?>
    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
       <SOAP-ENV:Body>
         <SOAP-ENV:Fault>
           <faultcode xsi:type = "xsd:string">400</faultcode>
           <faultstring xsi:type = "xsd:string">
             schema validation failed
   ============Validation Errors================
   Element '{http://schemas.xmlsoap.org/soap/envelope/}Body', attribute '{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd}Id': The attribute '{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd}Id' is not allowed.
   =============================================
           </faultstring>
         </SOAP-ENV:Fault>
       </SOAP-ENV:Body>
   </SOAP-ENV:Envelope>
   ```   
# 2. Импорт / Экспорт WSDL для применения проверок
1. Генерация Tyk API c использованием cli:
   ```bash
   ./smev apidef -w smev_core/fixtures/smev_fias/v_2_4/FNSFIASWS.wsdl -l /abc -u http://fias-stub:9090 >  abc.apidef.json 
   ```
2. Импорт файла в UI панели управления
3. Вызвать Postman запрос **_10. Вызов сгенерированного API_**
4. Экспорт файла в UI панели управления

# 3.Возможность использования инструмента локально, без обмена 
1. Валидация
   * Выполнить в корне проекта команду:
   ```bash
   ./smev validate -w smev_core/fixtures/smev_fias/v_2_4/FNSFIASWS.wsdl -m smev_core/fixtures/smev_fias/v_2_4/getStrictAddressZapros.soap.xml  -a getStrictAddressZapros --allow-undeclared-header-elements --allow-undeclared-body-attributes
   ```
   * Результат в консоли должен быть **message is valid**
   * Выполнить в корне проекта команду:
   ```bash
   ./smev validate -w smev_core/fixtures/smev_fias/v_2_4/FNSFIASWS.wsdl -m smev_core/fixtures/smev_fias/v_2_4/getStrictAddressZapros.soap.xml  -a getStrictAddressZapros --allow-undeclared-header-elements
   ```
   * Результат в консоли должен быть
   ```bash
   schema validation failed
   ============Validation Errors================
   Element '{http://schemas.xmlsoap.org/soap/envelope/}Body', attribute '{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd}Id': The attribute '{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd}Id' is not allowed.
   =============================================
   ```
   * Выполнить в корне проекта команду:
   ```bash
   ./smev validate -w smev_core/fixtures/smev_fias/v_2_4/FNSFIASWS.wsdl -m smev_core/fixtures/smev_fias/v_2_4/getStrictAddressZapros.soap.xml  -a getStrictAddressZapros --allow-undeclared-body-attributes
   ```
   * Результат в консоли должен быть
   ```bash
   schema validation failed
   ============Validation Errors================
   Element '{http://schemas.xmlsoap.org/soap/envelope/}Header': Character content is not allowed, because the content type is empty.
   Element '{http://schemas.xmlsoap.org/soap/envelope/}Header': Element content is not allowed, because the content type is empty.
   =============================================
   ```
2. Просмотр схемы операции
   * Выполнить в корне проекта команду:
   ```bash
   ./smev inspect -w smev_core/fixtures/smev_fias/v_2_4/FNSFIASWS.wsdl -a getStrictAddressZapros --allow-undeclared-header-elements --allow-undeclared-body-attributes
   ```
# 4. Возможность просмотра результатов проверки, с расшифровкой по: участникам потока, наименованию API, endpoint’ам и др. 
Показать UI панели управления