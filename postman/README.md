# Postman коллекция запросов
## Установка
1. импортировать коллекцию запросов [smev-tyk.postman_collection.json](./smev-tyk.postman_collection.json)
2. импортировать окружение [rocket@team25.bankingapi.ru.postman_environment.json](./rocket@team25.bankingapi.ru.postman_environment.json)
3. активировать в интерфейсе импортированное окружение rocket@team25.bankingapi.ru