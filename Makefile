TYK_VERSION := v4.1.0
PLUGIN_NAME := smev_tyk
PLUGIN_FILE_NAME := ${PLUGIN_NAME}.so
COMPILED_FILE_NAME := ${PLUGIN_NAME}_${TYK_VERSION}_linux_amd64.so
TEAM := team25

build: build-tyk build-stub build-cli

push: push-tyk push-stub

build-cli:
	/bin/sh -c "cd smev_cli && go build -o smev"

build-plugin:
	/bin/sh -c "cd smev_tyk && go mod tidy && go mod vendor"
	docker run --rm -v $(PWD)/smev_tyk:/plugin-source tykio/tyk-plugin-compiler:$(TYK_VERSION) $(PLUGIN_FILE_NAME)
	mv -f smev_tyk/$(COMPILED_FILE_NAME) tyk/middleware/$(PLUGIN_FILE_NAME)

build-tyk: build-plugin
	/bin/sh -c "cd tyk && docker build -t harbor.bankingapi.ru/$(TEAM)/tyk:v4.1.0-smev.1 ."

push-tyk: docker-login
	/bin/sh -c "docker push harbor.bankingapi.ru/$(TEAM)/tyk:v4.1.0-smev.1"

build-stub:
	/bin/sh -c "cd fias_stub && go build -o stub && chmod +x stub"
	/bin/sh -c "cd fias_stub && docker build -t harbor.bankingapi.ru/$(TEAM)/fias_stub:latest ."

push-stub:
	/bin/sh -c "docker push harbor.bankingapi.ru/$(TEAM)/fias_stub:latest"

push-helm:
	/bin/sh -c "helm push helm/smev-0.1.0.tgz oci://harbor.bankingapi.ru/team25/smev"

deploy:
	/bin/sh -c "helm repo add tyk-helm https://helm.tyk.io/public/helm/charts/ || true"
	/bin/sh -c "helm install mongo tyk-helm/simple-mongodb"
	/bin/sh -c "helm install redis tyk-helm/simple-redis"
	/bin/sh -c "helm install smev ./helm/charts/smev"
	/bin/sh -c "helm install tyk-pro ./helm/charts/tyk-pro -f helm/tyk.values.yaml --wait"

undeploy:
	/bin/sh -c "helm uninstall redis || true"
	/bin/sh -c "helm uninstall mongo || true"
	/bin/sh -c "helm uninstall smev || true"
	/bin/sh -c "helm uninstall tyk-pro || true"

password:
	kubectl get secret --namespace team25 tyk-pro-login-details -o jsonpath='{.data.TYK_PASS}' | base64 --decode | xargs -0 printf '%s\n'

start:
	docker-compose -p rocket up --remove-orphans --force-recreate --build -d

stop:
	docker-compose -p rocket down

docker-login:
	/bin/sh -c "docker login harbor.bankingapi.ru -u team25_team25 -p GFDCoQLuhb9k23wlUjz4Ugg8InQe0QO5"
	/bin/sh -c "helm registry login harbor.bankingapi.ru -u team25_team25 -p GFDCoQLuhb9k23wlUjz4Ugg8InQe0QO5"