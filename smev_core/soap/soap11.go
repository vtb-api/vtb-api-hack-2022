package soap

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"path/filepath"

	//"github.com/gosimple/slug"
	"github.com/lestrrat-go/libxml2"
	"github.com/lestrrat-go/libxml2/types"
	"github.com/lestrrat-go/libxml2/xsd"
	"github.com/pkg/errors"
	"github.com/rshlin/smev_core/wsdl"
)

const V11Namespace = `http://schemas.xmlsoap.org/wsdl/soap/`
const V11TransportNs = `http://schemas.xmlsoap.org/soap/http`

type V11Validator struct {
	wsdl       *wsdl.WSDL
	Operations map[Action]*OperationValidator
	options    *Options
}

type OperationValidator struct {
	Operation         *wsdl.BoundOperation
	RequestSchemaDoc  types.Node
	RequestSchema     *xsd.Schema
	ResponseSchemaDoc types.Node
	ResponseSchema    *xsd.Schema
}

func (v *V11Validator) ValidateRq(doc types.Document, soapAction Action) error {
	validator, ok := v.Operations[soapAction]
	if !ok {
		return fmt.Errorf("requested soap operation [%s] was not found", soapAction)
	}
	return validator.RequestSchema.Validate(doc, xsd.ValueVCCreate)
}

func (v *V11Validator) ValidateRs(doc types.Document, soapAction Action) error {
	validator, ok := v.Operations[soapAction]
	if !ok {
		return fmt.Errorf("requested soap operation [%s] was not found", soapAction)
	}
	return validator.ResponseSchema.Validate(doc, xsd.ValueVCCreate)
}

func NewSoap11Validator(wsdl *wsdl.WSDL, options *Options) (*V11Validator, error) {
	validator := V11Validator{wsdl: wsdl, options: options, Operations: make(map[Action]*OperationValidator, len(wsdl.BoundOperations))}
	if err := validator.init(); err != nil {
		return nil, errors.Wrap(err, "failed to create soap11 validator")
	}
	return &validator, nil
}

func (v *V11Validator) init() error {
	nsPrefixes := make(map[string]string, len(v.wsdl.NsPrefixes))
	nsMapping := make(map[string]string, len(v.wsdl.NsPrefixes))
	i := 0
	for k, v := range v.wsdl.NsPrefixes {
		ns := fmt.Sprintf("ns%d", i)
		i += 1
		nsPrefixes[ns] = v
		nsMapping[k] = ns
	}
	updateNsPrefix := func(ref *wsdl.Ref) {
		nsPrefix := nsMapping[ref.NsPrefix]
		if nsPrefix == "" {
			return
		}
		ref.NsPrefix = nsPrefix
	}
	for _, operation := range v.wsdl.BoundOperations {
		updateNsPrefix(operation.ResponseRef)
		updateNsPrefix(operation.RequestRef)
		for _, h := range operation.RequestHeaders {
			updateNsPrefix(h)
		}
		for _, h := range operation.ResponseHeaders {
			updateNsPrefix(h)
		}
		for _, f := range operation.Faults {
			updateNsPrefix(f)
		}
	}
	err := v.genOperationValidators(nsPrefixes)
	if err != nil {
		return errors.Wrap(err, "failed to create operation validators")
	}
	return nil
}

func (v *V11Validator) genOperationValidators(nsPrefixes map[string]string) error {
	for name, operation := range v.wsdl.BoundOperations {
		if err := v.genOperationSchema(nsPrefixes, operation, false); err != nil {
			return errors.Wrapf(err, "failed to generate rq schema for operation [%s]", name)
		}
		if err := v.genOperationSchema(nsPrefixes, operation, true); err != nil {
			return errors.Wrapf(err, "failed to generate rq schema for operation [%s]", name)
		}
	}

	return nil
}

func (v *V11Validator) genOperationSchema(prefixes map[string]string, operation *wsdl.BoundOperation, response bool) error {
	wrapper := operationTemplateData{
		NsPrefixes: prefixes,
		Wsdl:       v.wsdl,
		Response:   response,
		Options:    v.options,
		Operation:  operation,
	}
	buf, err := renderOperationSchema(&wrapper)
	if err != nil {
		return errors.Wrapf(err, "failed to render schema")
	}
	doc, err := libxml2.Parse(buf)
	if err != nil {
		return err
	}
	//d, err := doc.(types.Document).DocumentElement()
	//if err != nil {
	//	return err
	//}
	//e := d.(types.Element)
	//namespaces, err := e.GetNamespaces()
	//if err != nil {
	//	return err
	//}
	//for _, n := range namespaces {
	//	println(n.Prefix() + "=" + n.URI())
	//}
	schemaPath := v.getSchemaPath(response, operation)
	if err = ioutil.WriteFile(schemaPath, buf, 0644); err != nil {
		return errors.Wrapf(err, "failed to flush operation schema file %s", schemaPath)
	}
	schema, err := xsd.ParseFromFile(schemaPath)
	//schema, err := xsd.Parse(buf, xsd.WithPath())
	if err != nil {
		println(string(buf))
		return errors.Wrapf(err, "failed to parse rendered schema")
	}
	var soapAction Action
	if operation.SoapAction != "" {
		soapAction = Action(operation.SoapAction)
	} else {
		soapAction = Action(operation.Name)
	}
	ov, ok := v.Operations[soapAction]
	if !ok {
		ov = &OperationValidator{
			Operation: operation,
		}
	}
	if response {
		ov.ResponseSchemaDoc = doc
		ov.ResponseSchema = schema
	} else {
		ov.RequestSchemaDoc = doc
		ov.RequestSchema = schema
	}
	v.Operations[soapAction] = ov

	return nil
}

func (v *V11Validator) getSchemaPath(response bool, operation *wsdl.BoundOperation) string {
	var postfix string
	if response {
		postfix = "response.envelope.xsd"
	} else {
		postfix = "request.envelope.xsd"
	}
	wsdlDir := filepath.Dir(v.wsdl.WsdlUrl)
	operationEnvelopeSchemaName := fmt.Sprintf("%s-%s", operation.Name, postfix)
	schemaPath := filepath.Join(wsdlDir, operationEnvelopeSchemaName)
	return schemaPath
}

type operationTemplateData struct {
	NsPrefixes map[string]string
	Wsdl       *wsdl.WSDL
	Response   bool
	Options    *Options
	Operation  *wsdl.BoundOperation
}

func renderOperationSchema(wrapper *operationTemplateData) ([]byte, error) {
	data := new(bytes.Buffer)
	err := tmpl.Execute(data, wrapper)
	if err != nil {
		return nil, err
	}
	return data.Bytes(), nil
}
