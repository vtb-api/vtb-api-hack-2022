package soap

import "text/template"

type FaultTemplateData struct {
	Code    int
	Message string
}

var FaultTmpl = template.Must(template.New("fault").Parse(ftmpl))

var ftmpl = `<?xml version = '1.0' encoding = 'UTF-8'?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance" xmlns:xsd="http://www.w3.org/1999/XMLSchema">
   <SOAP-ENV:Body>
      <SOAP-ENV:Fault>
         <faultcode xsi:type = "xsd:string">{{ .Code }}</faultcode>
         <faultstring xsi:type = "xsd:string">
			   {{ .Message }}
         </faultstring>
      </SOAP-ENV:Fault>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
`
