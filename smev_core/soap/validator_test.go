package soap

import (
	"github.com/lestrrat-go/libxml2"
	"github.com/rshlin/smev_core/wsdl"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"testing"
)

func TestNewSoap11Validator(t *testing.T) {
	wsdl11, err := wsdl.NewWsdlFromFile("../fixtures/smev_fias/v_2_4/FNSFIASWS.wsdl", &wsdl.Options{})
	if !assert.NoError(t, err, "should create wsdl object") {
		return
	}
	if err = wsdl11.FlushRootSchemas(); !assert.NoError(t, err, "should flush root schemas") {
		return
	}
	opts := Options{
		AllowUndeclaredEnvelopeElements:   false,
		AllowUndeclaredEnvelopeAttributes: false,
		AllowUndeclaredHeaderElements:     false,
		AllowUndeclaredHeaderAttributes:   false,
		AllowUndeclaredFaults:             false,
		AllowUndeclaredBodyAttributes:     false,
	}
	validator, err := NewSoap11Validator(wsdl11, &opts)
	if !assert.NoError(t, err, "should create validator") {
		return
	}
	xmlSrc, err := ioutil.ReadFile("../fixtures/smev_fias/v_2_4/getStrictAddressZapros.soap.xml")
	if !assert.NoError(t, err, "failed to read xml file") {
		return
	}
	doc, err := libxml2.Parse(xmlSrc)
	if !assert.NoError(t, err, "failed to parse xml") {
		return
	}
	defer doc.Free()
	if err := validator.ValidateRq(doc, "getStrictAddressZapros"); !assert.Errorf(t, err, "should fail validating envelope with undeclared wsi policy") {
		return
	}
	opts.AllowUndeclaredBodyAttributes = true
	opts.AllowUndeclaredHeaderElements = true
	validator, err = NewSoap11Validator(wsdl11, &opts)
	if !assert.NoError(t, err, "should create validator") {
		return
	}
	if err = validator.ValidateRq(doc, "getStrictAddressZapros"); !assert.NoError(t, err, "should validate envelope with relaxed opts") {
		return
	}
}
