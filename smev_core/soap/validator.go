package soap

import (
	"fmt"
	"github.com/lestrrat-go/libxml2/types"
	"github.com/rshlin/smev_core/wsdl"
)

type Action string

type Validator interface {
	ValidateRq(doc types.Document, soapAction Action) error
	ValidateRs(doc types.Document, soapAction Action) error
}

type Options struct {
	AllowUndeclaredEnvelopeElements   bool `mapstructure:"allow_undeclared_envelope_elements" json:"allow_undeclared_envelope_elements"`
	AllowUndeclaredEnvelopeAttributes bool `mapstructure:"allow_undeclared_envelope_attributes" json:"allow_undeclared_envelope_attributes"`
	AllowUndeclaredHeaderElements     bool `mapstructure:"allow_undeclared_header_elements" json:"allow_undeclared_header_elements"`
	AllowUndeclaredHeaderAttributes   bool `mapstructure:"allow_undeclared_header_attributes" json:"allow_undeclared_header_attributes"`
	AllowUndeclaredFaults             bool `mapstructure:"allow_undeclared_faults" json:"allow_undeclared_faults"`
	AllowUndeclaredBodyAttributes     bool `mapstructure:"allow_undeclared_body_attributes" json:"allow_undeclared_body_attributes"`
}

func NewSoapValidator(wsdl *wsdl.WSDL, options *Options) (Validator, error) {
	switch wsdl.TargetBinding.SoapNs {
	case V11Namespace:
		return NewSoap11Validator(wsdl, options)
	case V12Namespace:
		return nil, fmt.Errorf("soap v1.2 is not supported for validation")
	default:
		return nil, fmt.Errorf("unsupported soap version: %s", wsdl.TargetBinding.SoapNs)
	}
}
