# SMEV core
Ядро логики валидации.

На каждую объявленную операцию сервиса генерируется 2 XSD схемы SOAP конверта (запрос и ответ).

Сообщения валидируются с использованием библиотеки [libxml2](https://gitlab.gnome.org/GNOME/libxml2), которая написана на языке C.

## Детали реализации
1. Support only Document/Literal wrapped services, which are WS-I compliant
2. Support:
   * WSDL 1.1
   * XML Schema 1.0
   * SOAP 1.1

## Roadmap
* WSDL 1.2
* SOAP 1.2
* WSS Policies
* WSS Signature validation
* GOST encryption