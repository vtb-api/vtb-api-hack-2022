module github.com/rshlin/smev_core

go 1.15

require (
	github.com/gosimple/slug v1.13.1
	github.com/lestrrat-go/libxml2 v0.0.0-20201123224832-e6d9de61b80d
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.8.0
	gopkg.in/xmlpath.v1 v1.0.0-20140413065638-a146725ea6e7 // indirect
	launchpad.net/gocheck v0.0.0-20140225173054-000000000087 // indirect
	launchpad.net/xmlpath v0.0.0-20130614043138-000000000004 // indirect
)
