// Package wsi
// for more information see
// http://www.ws-i.org/
// https://www.ibm.com/docs/en/zvse/6.2?topic=SSB27H_6.2.0%2Ffa2ws_ovw_soap_syntax_lit.html
// https://www.ibm.com/docs/en/baw/19.x?topic=files-wsdl-binding-styles
package wsi

import (
	"fmt"
	"github.com/lestrrat-go/libxml2/types"
	"github.com/lestrrat-go/libxml2/xpath"
	"github.com/rshlin/smev_core/common"
	xpathUtils "github.com/rshlin/smev_core/xpath"
)

func ValidateLiteralUse(input types.Node) error {
	xPathResult := xpath.NodeList(input.Find("@use"))
	attribute, err := xPathResult[0].Literal()
	if err != nil {
		return err
	}
	val := common.GetAttributeValue(attribute)
	if val != "literal" {
		return fmt.Errorf("encoded use is not supported. exclude encoded use in the targeted wsdl binding")
	}
	return nil
}

func ValidateSoapOperationStyle(operation types.Node) error {
	style, err := xpathUtils.AttrValue(operation, "style")
	if err != nil {
		return err
	}
	if style != "document" {
		return fmt.Errorf("only document style soap operations are supported")
	}
	return nil
}

//ValidatedMessagePart returns single Part or error
func ValidatedMessagePart(message types.Node) (types.Node, error) {
	name := xpath.String(message.Find("@name"))
	parts := xpathUtils.FindMessageParts(message, name)
	if len(parts) == 0 {
		return nil, fmt.Errorf("no message parts was found")
	}
	if len(parts) > 1 {
		return nil, fmt.Errorf("found more than one message part")
	}
	return parts[0], nil
}
