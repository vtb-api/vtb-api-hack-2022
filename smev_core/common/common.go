package common

import "strings"

func Stripns(attribute string) string {
	xsdType := GetAttributeValue(attribute)
	r := strings.Split(xsdType, ":")
	t := r[0]

	if len(r) == 2 {
		t = r[1]
	}

	return t
}

func GetAttributeValue(attribute string) string {
	r := strings.Split(attribute, "=")
	if len(r) == 1 {
		return r[0]
	}
	return strings.Trim(r[1], "\"")
}
