package wsdl

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewWsdlFromFile(t *testing.T) {
	_, err := NewWsdlFromFile("../fixtures/smev_fias/v_2_4/FNSFIASWS.wsdl", &Options{})
	if !assert.NoError(t, err, "should create wsdl object") {
		return
	}
}

func TestTargetService(t *testing.T) {
	wsdl, err := NewWsdlFromFile("../fixtures/smev_fias/v_2_4/FNSFIASWS.wsdl", &Options{})
	if !assert.NoError(t, err, "should create wsdl object") {
		return
	}
	if !assert.Truef(t, wsdl.TargetService != nil, "should create target service") {
		return
	}
	if !assert.Equalf(t, "FNSFIASWSSoap11", wsdl.TargetService.BindingName, "should identify target service without options") {
		return
	}
	wsdl, err = NewWsdlFromFile("../fixtures/smev_fias/v_2_4/FNSFIASWS.wsdl", &Options{TargetService: "FNSFIASWS_24"})
	if !assert.NoError(t, err, "should create wsdl object") {
		return
	}
	if !assert.Truef(t, wsdl.TargetService != nil, "should create target service") {
		return
	}
	if !assert.Equalf(t, "FNSFIASWSSoap11", wsdl.TargetService.BindingName, "should identify target service with service option") {
		return
	}
	wsdl, err = NewWsdlFromFile("../fixtures/smev_fias/v_2_4/FNSFIASWS.wsdl", &Options{TargetPort: "FNSFIASWS_24Port"})
	if !assert.NoError(t, err, "should create wsdl object") {
		return
	}
	if !assert.Truef(t, wsdl.TargetService != nil, "should create target service") {
		return
	}
	if !assert.Equalf(t, "FNSFIASWSSoap11", wsdl.TargetService.BindingName, "should identify target service with port option") {
		return
	}
	wsdl, err = NewWsdlFromFile("../fixtures/smev_fias/v_2_4/FNSFIASWS.wsdl", &Options{TargetService: "FNSFIASWS_24", TargetPort: "FNSFIASWS_24Port"})
	if !assert.NoError(t, err, "should create wsdl object") {
		return
	}
	if !assert.Truef(t, wsdl.TargetService != nil, "should create target service") {
		return
	}
	if !assert.Equalf(t, "FNSFIASWSSoap11", wsdl.TargetService.BindingName, "should identify target service with service and target options") {
		return
	}
	wsdl, err = NewWsdlFromFile("../fixtures/smev_fias/v_2_4/FNSFIASWS_two_services.wsdl", &Options{})
	if !assert.Errorf(t, err, "should fail without specified service opts") {
		return
	}
	wsdl, err = NewWsdlFromFile("../fixtures/smev_fias/v_2_4/FNSFIASWS_two_services.wsdl", &Options{TargetService: "FNSFIASWS_24"})
	if !assert.NoError(t, err, "should create wsdl object") {
		return
	}
	if !assert.Truef(t, wsdl.TargetService != nil, "should create target service") {
		return
	}
	if !assert.Equalf(t, "FNSFIASWSSoap11", wsdl.TargetService.BindingName, "should identify target service with service and target options") {
		return
	}
	wsdl, err = NewWsdlFromFile("../fixtures/smev_fias/v_2_4/FNSFIASWS_two_service_ports.wsdl", &Options{})
	if !assert.Errorf(t, err, "should fail without specified port opts") {
		return
	}
	wsdl, err = NewWsdlFromFile("../fixtures/smev_fias/v_2_4/FNSFIASWS_two_service_ports.wsdl", &Options{TargetPort: "FNSFIASWS_24Port"})
	if !assert.NoError(t, err, "should create wsdl object") {
		return
	}
	if !assert.Truef(t, wsdl.TargetService != nil, "should create target service") {
		return
	}
	if !assert.Equalf(t, "FNSFIASWSSoap11", wsdl.TargetService.BindingName, "should identify target service with service and target options") {
		return
	}
}
func TestTargetBindings(t *testing.T) {
	wsdl, err := NewWsdlFromFile("../fixtures/smev_fias/v_2_4/FNSFIASWS.wsdl", &Options{})
	if !assert.NoError(t, err, "should create wsdl object") {
		return
	}
	if !assert.Truef(t, wsdl.TargetBinding != nil, "should create target binding") {
		return
	}
	wsdl, err = NewWsdlFromFile("../fixtures/smev_fias/v_2_4/FNSFIASWS_missing_binding.wsdl", &Options{})
	if !assert.Error(t, err, "should fail without binding") {
		return
	}
}
func TestRootSchemas(t *testing.T) {
	wsdl, err := NewWsdlFromFile("../fixtures/smev_fias/v_2_4/FNSFIASWS.wsdl", &Options{})
	if !assert.NoError(t, err, "should create wsdl object") {
		return
	}
	if !assert.True(t, wsdl.RootSchemas != nil, "should create rootSchemas") {
		return
	}
	if !assert.Truef(t, len(wsdl.RootSchemas) == 1, "should discover single root schema") {
		return
	}
	if !assert.NotNil(t, wsdl.RootSchemas["http://ws.unisoft/"], "should have root schema with specified namespace") {
		return
	}
}
func TestPortType(t *testing.T) {
	wsdl, err := NewWsdlFromFile("../fixtures/smev_fias/v_2_4/FNSFIASWS.wsdl", &Options{})
	if !assert.NoError(t, err, "should create wsdl object") {
		return
	}
	if !assert.Truef(t, wsdl.TargetPortType != nil, "shoud create target port type") {
		return
	}
}
