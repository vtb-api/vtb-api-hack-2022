// Package wsdl https://www.w3.org/TR/2001/NOTE-wsdl-20010315
package wsdl

import (
	"fmt"
	"github.com/gosimple/slug"
	"github.com/lestrrat-go/libxml2/dom"
	"github.com/lestrrat-go/libxml2/types"
	"github.com/lestrrat-go/libxml2/xpath"
	"github.com/lestrrat-go/libxml2/xsd"
	"github.com/pkg/errors"
	"github.com/rshlin/smev_core/common"
	"github.com/rshlin/smev_core/wsi"
	xpathUtils "github.com/rshlin/smev_core/xpath"
	"net/url"
	"path"
	"path/filepath"
	"strings"
)

const V11Ns = "http://schemas.xmlsoap.org/wsdl/"

type wsdl11 struct {
	WSDL
}

func (w *wsdl11) init() error {
	err := validateWsdl(w.Doc)
	if err != nil {
		return errors.Wrapf(err, "failed to verify xml validity of wsdl document")
	}

	schemas, err := w.getSchemas()
	if err != nil {
		return err
	}
	w.RootSchemas = schemas

	messages, err := w.getMessages()
	if err != nil {
		return err
	}
	w.messages = messages

	targetService, err := w.getTargetService()
	if err != nil {
		return err
	}
	w.TargetService = targetService

	bindings, err := w.getTargetBindings()
	if err != nil {
		return err
	}
	w.TargetBinding = bindings

	port, err := w.getTargetPortType()
	if err != nil {
		return err
	}
	w.TargetPortType = port

	boundOperations, err := w.getBoundOperations()
	if err != nil {
		return err
	}
	w.BoundOperations = boundOperations

	if err = w.resolveNamespaces(); err != nil {
		return errors.Wrapf(err, "failed to resolve ns prefixes")
	}
	return nil
}

func validateWsdl(wsdl types.Document) error {
	return wsdl11xsd.Validate(wsdl, xsd.ValueVCCreate)
}

func (w *wsdl11) getBoundOperations() (map[string]*BoundOperation, error) {
	operationNodes := xpathUtils.FindBindingOperationsByName(w.Doc, w.TargetBinding.Name)
	ops := make(map[string]*BoundOperation, len(operationNodes))
	for _, opNode := range operationNodes {
		op, err := w.extractOperationInfo(opNode)
		if err != nil {
			return nil, err
		}
		ops[op.Name] = op
	}
	return ops, nil
}

func (w *WSDL) getMessages() (map[string]*Message, error) {
	nodeList := xpathUtils.FindMessages(w.Doc)
	result := make(map[string]*Message, len(nodeList))
	for _, node := range nodeList {
		part, err := wsi.ValidatedMessagePart(node)
		if err != nil {
			return nil, err
		}
		name, err := xpathUtils.Name(node)
		if err != nil {
			return nil, err
		}
		msg := Message{
			Name: name,
		}
		partName, err := xpathUtils.Name(part)
		if err != nil {
			return nil, err
		}
		partElement, err := part.Find("@element")
		if err != nil {
			return nil, err
		}
		partType, err := xpathUtils.Type(part)
		if err != nil {
			return nil, err
		}
		msg.Part = &MessagePart{
			Name:    partName,
			Element: w.getRef(partElement.String()),
			Type:    partType,
		}

		result[msg.Name] = &msg
	}

	return result, nil
}

func (w *WSDL) getTargetBindings() (*TargetBinding, error) {
	targetBindingName := w.TargetService.BindingName
	soapBindingNs, err := xpathUtils.FindSoapBindingNs(w.Doc, targetBindingName)
	if err != nil {
		return nil, err
	}

	binding, err := xpathUtils.FindBindingByName(w.Doc, targetBindingName)
	if err != nil {
		return nil, err
	}
	bindingType, err := xpathUtils.Type(binding)
	if err != nil {
		return nil, err
	}
	bindingType = common.Stripns(bindingType)
	targetBindingName, err = xpathUtils.Name(binding)
	if err != nil {
		return nil, err
	}

	targetBinding := TargetBinding{
		SoapNs: soapBindingNs,
		Name:   targetBindingName,
		Type:   bindingType,
		Doc:    binding,
	}
	return &targetBinding, nil
}

func (w *WSDL) getSchemas() (map[string]*Schema, error) {
	schemaNodes := xpathUtils.FindSchemas(w.Doc)
	schemas := make(map[string]*Schema)
	for _, node := range schemaNodes {
		cpy, err := node.Copy()
		//imports := xpath.NodeList(cpy.Find(`.//*[local-name()="import" and namespace-uri()="http://www.w3.org/2001/XMLSchema"]`))
		//for _, i := range imports {
		//	e := i.(types.Element)
		//	schemaLocationAttr, err := e.GetAttribute("schemaLocation")
		//	if err != nil {
		//		continue
		//	}
		//	if _, err = url.ParseRequestURI(schemaLocationAttr.Value()); err == nil {
		//		continue
		//	}
		//	if filepath.IsAbs(schemaLocationAttr.Value()) {
		//		continue
		//	}
		//	schemaLocationAttr.SetNodeValue(updatedSchemaLocation(e, w.WsdlUrl))
		//}
		if err != nil {
			return nil, err
		}
		ns, err := xpathUtils.TargetNameSpace(node)

		schemaPath := slug.Make(ns) + ".xsd"
		schema := Schema{
			Namespace:      ns,
			SchemaLocation: schemaPath,
			Doc:            cpy,
		}
		schemas[ns] = &schema
	}
	return schemas, nil
}

func updatedSchemaLocation(schema types.Element, wsdlLocation string) string {
	schemaLocation, err := schema.GetAttribute("schemaLocation")
	if err != nil {
		return ""
	}
	value := schemaLocation.Value()
	if _, err = url.ParseRequestURI(value); err == nil {
		return value
	}
	if filepath.IsAbs(value) {
		return value
	}
	updatedPath := path.Join(wsdlLocation, "../"+value)

	return updatedPath
}

func (w *WSDL) extractOperationInfo(opNode types.Node) (*BoundOperation, error) {
	nameResult, err := opNode.Find("@name")
	if err != nil {
		return nil, errors.Wrapf(err, "failed to locate operation name")
	}
	name := nameResult.String()
	portTypeOperation, ok := w.TargetPortType.Operations[name]
	if !ok {
		return nil, fmt.Errorf("operation [%s] was not found in target port type [%s]", name, w.TargetPortType.Name)
	}
	op := BoundOperation{Operation: *portTypeOperation}
	soapOperationResult := xpath.NodeList(opNode.Find(`.//*[local-name()="operation"]`))
	if len(soapOperationResult) == 0 {
		return nil, fmt.Errorf("failed to locate soap operation for wsdl operation %s", name)
	}
	soapOperation := soapOperationResult[0]
	if err = wsi.ValidateSoapOperationStyle(soapOperation); err != nil {
		return nil, errors.Wrapf(err, "failed to validate soap operation [%s] style", name)
	}
	soapAction, err := xpathUtils.AttrValue(soapOperation, "soapAction")
	if err != nil {
		return nil, errors.Wrapf(err, "failed to locate soapAction attribute for operation %s", name)
	}
	op.SoapAction = soapAction

	inputResult := xpath.NodeList(opNode.Find(`.//*[local-name()="input"]`))
	if len(inputResult) == 0 {
		return nil, fmt.Errorf("empty input for operation %s", name)
	}

	input := inputResult[0]

	err = validateUse(input)
	if err != nil {
		return nil, errors.Wrapf(err, "failed validation of soap input use for operation %s", name)
	}

	outputResult := xpath.NodeList(opNode.Find(`.//*[local-name()="output"]`))
	if len(outputResult) == 0 {
		return nil, fmt.Errorf("empty output for operation %s", name)
	}

	output := outputResult[0]
	err = validateUse(output)
	if err != nil {
		return nil, errors.Wrapf(err, "failed validation of soap output use for operation %s", name)
	}

	headers, err := w.getHeaders(input)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to extract request headers for operation %s", name)
	}
	op.RequestHeaders = headers

	headers, err = w.getHeaders(output)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to extract response headers for operation %s", name)
	}
	op.ResponseHeaders = headers

	return &op, nil
}

func (w *WSDL) getHeaders(io types.Node) ([]*Ref, error) {
	messages := xpath.NodeList(io.Find(`.//*[local-name()="header"]/@message`))
	refs := make([]*Ref, len(messages))
	for i, message := range messages {
		refs[i] = w.getRef(message.String())
	}
	return refs, nil
}

func (w *WSDL) getFaults(io types.Node) ([]*Ref, error) {
	messages := xpath.NodeList(io.Find(`.//*[local-name()="fault"]/@message`))
	refs := make([]*Ref, len(messages))
	for i, message := range messages {
		msgRef := w.getRef(message.String())
		faultRef, ok := w.messages[msgRef.Element]
		if !ok {
			return nil, fmt.Errorf("failed to locate fault definition for message %s", msgRef.Element)
		}
		refs[i] = faultRef.Part.Element
	}
	return refs, nil
}

func validateUse(io types.Node) error {
	childNodes, err := io.ChildNodes()
	if err != nil {
		return fmt.Errorf("couldn't locate child elements")
	}
	for _, i := range childNodes {
		if _, ok := i.(*dom.Element); ok {
			err := wsi.ValidateLiteralUse(i)
			if err != nil {
				return errors.Wrapf(err, "input/output %s does not have literal use", common.Stripns(i.NodeName()))
			}
		}
	}
	return nil
}

func (w *WSDL) getTargetPortType() (*TargetPortType, error) {
	portType, err := xpathUtils.FindPortTypeByName(w.Doc, w.TargetBinding.Type)
	if err != nil {
		return nil, err
	}
	println(portType.String())
	portName, err := xpathUtils.Name(portType)
	if err != nil {
		return nil, err
	}

	ops := xpath.NodeList(portType.Find(`.//*[local-name()="operation"]`))
	operations := make(map[string]*Operation, len(ops))
	for _, opNode := range ops {
		name, err := xpathUtils.Name(opNode)
		if err != nil {
			return nil, errors.Wrapf(err, "failed to extract operation name")
		}
		op := &Operation{
			Name: name,
		}
		inputMsgRef := xpath.String(opNode.Find(`.//*[local-name()="input"]/@message`))
		if inputMsgRef == "" {
			return nil, fmt.Errorf("failed to extract [%s] operation input message", name)
		}
		ref := w.getRef(inputMsgRef)
		message, ok := w.messages[ref.Element]
		if !ok {
			return nil, fmt.Errorf("couldn't locate referenced message for %s:%s", ref.NsPrefix, ref.Element)
		}
		op.RequestRef = message.Part.Element

		outputMsgRef := xpath.String(opNode.Find(`.//*[local-name()="output"]/@message`))
		if outputMsgRef == "" {
			return nil, fmt.Errorf("failed to extract [%s] operation output message", name)
		}
		ref = w.getRef(outputMsgRef)
		message, ok = w.messages[ref.Element]
		if !ok {
			return nil, fmt.Errorf("couldn't locate referenced message for %s:%s", ref.NsPrefix, ref.Element)
		}
		op.ResponseRef = message.Part.Element

		faults, err := w.getFaults(opNode)
		if err != nil {
			return nil, errors.Wrapf(err, "failed to extract faults for operation %s", name)
		}
		op.Faults = faults

		operations[op.Name] = op
	}

	targetPortType := TargetPortType{
		Name:       portName,
		Doc:        portType,
		Operations: operations,
	}

	return &targetPortType, nil
}

func (w *WSDL) getRef(attribute string) *Ref {
	value := common.GetAttributeValue(attribute)
	r := strings.Split(value, ":")
	if len(r) == 2 {
		w.NsPrefixes[r[0]] = w.TnsPrefix
		return &Ref{
			NsPrefix: r[0],
			Element:  r[1],
		}
	}
	return &Ref{Element: r[0]}
}

func (w *WSDL) getTargetService() (*TargetService, error) {
	targetServiceName := w.WsdlOptions.TargetService
	targetPort := w.WsdlOptions.TargetPort

	serviceDoc, err := xpathUtils.FindServiceByName(w.Doc, targetServiceName)
	if err != nil {
		return nil, err
	}
	targetServiceName, err = xpathUtils.Name(serviceDoc)
	if err != nil {
		return nil, err
	}

	bindingName, err := xpathUtils.FindServicePortBindingByName(serviceDoc, targetServiceName, targetPort)
	if err != nil {
		return nil, err
	}

	targetService := TargetService{
		Name:        targetServiceName,
		Doc:         serviceDoc,
		BindingName: bindingName,
	}

	return &targetService, nil
}
