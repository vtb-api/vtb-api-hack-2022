package wsdl

import (
	"fmt"
	"github.com/lestrrat-go/libxml2"
	"github.com/lestrrat-go/libxml2/types"
	"github.com/pkg/errors"
	xpathUtils "github.com/rshlin/smev_core/xpath"
	"io/ioutil"
	"net/http"
	"path"
	"path/filepath"
)

type WSDL struct {
	WsdlOptions     *Options
	rawWsdl         []byte
	WsdlUrl         string
	TargetService   *TargetService
	TargetBinding   *TargetBinding
	TargetPortType  *TargetPortType
	BoundOperations map[string]*BoundOperation
	NsPrefixes      map[string]string
	Doc             types.Document
	RootSchemas     map[string]*Schema
	messages        map[string]*Message
	Tns             string
	TnsPrefix       string
}

type TargetService struct {
	Name        string
	Doc         types.Node
	BindingName string
}

type TargetPortType struct {
	Name       string
	Doc        types.Node
	Operations map[string]*Operation
}

type TargetBinding struct {
	Name   string
	SoapNs string
	Type   string
	Doc    types.Node
}

type MessagePart struct {
	Name    string
	Element *Ref
	Type    string
}

type Message struct {
	Name string
	Part *MessagePart
}

type Schema struct {
	Namespace      string
	SchemaLocation string
	Doc            types.Node
	Imports        map[string]*Schema
}

type Operation struct {
	Name        string
	Faults      []*Ref
	RequestRef  *Ref
	ResponseRef *Ref
}

type Ref struct {
	NsPrefix string
	Element  string
}

type BoundOperation struct {
	Operation
	SoapAction      string
	RequestHeaders  []*Ref
	ResponseHeaders []*Ref
}

type Options struct {
	TargetService string `mapstructure:"target_service" json:"target_service"`
	TargetPort    string `mapstructure:"target_port" json:"target_port"`
}

func NewWsdlFromFile(path string, options *Options) (*WSDL, error) {
	buf, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	absPath, err := filepath.Abs(path)
	if err != nil {
		return nil, err
	}
	wsdl := &WSDL{
		rawWsdl:     buf,
		WsdlUrl:     absPath,
		WsdlOptions: options,
	}

	wsdl, err = wsdl.init()
	if err != nil {
		return nil, err
	}
	return wsdl, nil
}

// NewWsdlFromMem workingDirectory - directory where imported xsd files can be found. if wsdl does not have imports pick tmp folder path
func NewWsdlFromMem(buf []byte, workingDirectory string, options *Options) (*WSDL, error) {
	wsdl := &WSDL{
		rawWsdl:     buf,
		WsdlUrl:     path.Join(workingDirectory, "wsdl.wsdl"),
		WsdlOptions: options,
	}

	wsdl, err := wsdl.init()
	if err != nil {
		return nil, err
	}
	return wsdl, nil
}

func NewWsdlFromUrl(url string, validationOptions *Options) (*WSDL, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	buf, err := ioutil.ReadAll(resp.Body)

	wsdl := &WSDL{
		rawWsdl:     buf,
		WsdlUrl:     url,
		WsdlOptions: validationOptions,
	}

	wsdl, err = wsdl.init()
	if err != nil {
		return nil, err
	}
	return wsdl, nil
}

func (w *WSDL) init() (*WSDL, error) {
	w.NsPrefixes = make(map[string]string)
	doc, err := libxml2.Parse(w.rawWsdl)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse wsdl")
	}
	w.Doc = doc
	tns, err := xpathUtils.AttrValue(doc, "targetNamespace")
	if err != nil {
		return nil, errors.Wrapf(err, "failed to locate targetNamespace")
	}
	w.Tns = tns
	element, err := doc.DocumentElement()
	if err != nil {
		return nil, err
	}
	tnsPrefix, err := element.LookupNamespacePrefix(tns)
	if err != nil {
		return nil, err
	}
	w.TnsPrefix = tnsPrefix

	err = w.denormalizeWsdl()
	if err != nil {
		return nil, err
	}
	_, err = w.Doc.LookupNamespacePrefix(V12Ns)
	if err == nil {
		return nil, fmt.Errorf("wsdl 1.2 is not supported")
	}
	_, err = w.Doc.LookupNamespacePrefix(V11Ns)
	if err == nil {
		w11 := wsdl11{WSDL: *w}
		err = w11.init()
		if err != nil {
			return nil, err
		}
		return &w11.WSDL, nil
	}
	return nil, fmt.Errorf("unsupported wsdl version")
}

func (w *WSDL) denormalizeWsdl() error {
	//TODO
	return nil
}

var stopWalk = errors.New("stop walk")

func (w *WSDL) resolveNamespaces() error {
	for prefix, _ := range w.NsPrefixes {
		err := w.Doc.Walk(func(node types.Node) error {
			ns, err := node.LookupNamespaceURI(prefix)
			if err == nil && ns != "" {
				w.NsPrefixes[prefix] = ns
				return stopWalk
			}
			return nil
		})
		if err != nil {
			return errors.Wrapf(err, "unexpected error")
		}
		if w.NsPrefixes[prefix] == "" {
			return fmt.Errorf("failed to resolve namespace for prefix [%s]", prefix)
		}
	}
	return nil
}

func (w *WSDL) FlushRootSchemas() error {
	wsdlDir := filepath.Dir(w.WsdlUrl)
	for _, schema := range w.RootSchemas {
		if err := schema.flushToFS(wsdlDir); err != nil {
			return errors.Wrapf(err, "failed to flush root schemas")
		}
	}
	return nil
}

func (s *Schema) flushToFS(wsdlDir string) error {
	err := ioutil.WriteFile(filepath.Join(wsdlDir, s.SchemaLocation), []byte(s.Doc.String()), 0644)
	if err != nil {
		return errors.Wrapf(err, "failed to flush schema [%s] to file [%s]", s.Namespace, s.SchemaLocation)
	}
	return nil
}
