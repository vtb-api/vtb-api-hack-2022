package smev_core

import (
	"github.com/lestrrat-go/libxml2"
	"github.com/lestrrat-go/libxml2/xsd"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"testing"
)

func TestGetStrictAddressZapros(t *testing.T) {
	schemaFile := "fixtures/smev_fias/v_2_4/soap-envelope.smev.v2.4.3.getStrictAddressZapros.xsd"
	schemaSrc, err := ioutil.ReadFile(schemaFile)
	if !assert.NoError(t, err, "failed to read xsd file") {
		return
	}
	schema, err := xsd.Parse(schemaSrc, xsd.WithURI(schemaFile))
	if !assert.NoError(t, err, "failed to parse schema") {
		return
	}
	defer schema.Free()

	xmlSrc, err := ioutil.ReadFile("fixtures/smev_fias/v_2_4/getStrictAddressZapros.soap.xml")
	if !assert.NoError(t, err, "failed to read xml file") {
		return
	}
	doc, err := libxml2.Parse(xmlSrc)
	if !assert.NoError(t, err, "failed to parse xml") {
		return
	}
	defer doc.Free()
	err = schema.Validate(doc, xsd.ValueVCCreate)

	if !assert.NoError(t, err, "schema.Validate failed") {
		return
	}

	xmlSrc, err = ioutil.ReadFile("fixtures/smev_fias/v_2_4/getStrictAddressZapros.invalid.soap.xml")
	if !assert.NoError(t, err, "failed to read xml file") {
		return
	}
	doc2, err := libxml2.Parse(xmlSrc)
	if !assert.NoError(t, err, "failed to parse xml") {
		return
	}
	defer doc2.Free()
	err = schema.Validate(doc2, xsd.ValueVCCreate)

	if !assert.Errorf(t, err, "schema.Validate failed") {
		return
	}
}
