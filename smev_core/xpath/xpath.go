package xpath

import (
	"fmt"
	"github.com/lestrrat-go/libxml2/types"
	"github.com/lestrrat-go/libxml2/xpath"
	"github.com/rshlin/smev_core/common"
	"strings"
)

const xServices = `/*[local-name()="definitions"]/*[local-name()="service"]`
const xServiceByName = `/*[local-name()="definitions"]/*[local-name()="service" and @name="%s"]`
const xServicePortBindingByName = `/*[local-name()="definitions"]/*[local-name()="service" and @name="%s"]/*[local-name()="port" and @name="%s"]/@binding`
const xServicePortBinding = `/*[local-name()="definitions"]/*[local-name()="service" and @name="%s"]/*[local-name()="port"]/@binding`
const xSchemas = `/*[local-name()="definitions"]/*[local-name()="types"]/*[local-name()="schema"]`
const xSoapBinding = `/*[local-name()="definitions"]/*[local-name()="binding" and @name="%s"]/*[local-name()="binding"]`
const xBindingByName = `/*[local-name()="definitions"]/*[local-name()="binding" and @name="%s"]`
const xPortType = `/*[local-name()="definitions"]/*[local-name()="portType" and @name="%s"]`
const xMessages = `/*[local-name()="definitions"]/*[local-name()="message"]`
const xMessagePartsByMessageName = `/*[local-name()="definitions"]/*[local-name()="message" and @name="%s"]/*[local-name()="part"]`
const xBindingOperationsByName = `/*[local-name()="definitions"]/*[local-name()="binding" and @name="%s"]/*[local-name()="operation"]`

func Name(node types.Node) (string, error) {
	return AttrValue(node, "name")
}

func Type(node types.Node) (string, error) {
	return AttrValue(node, "type")
}

func TargetNameSpace(schema types.Node) (string, error) {
	return AttrValue(schema, "targetNamespace")
}

func AttrValue(node types.Node, attrName string) (string, error) {
	name, err := node.Find(fmt.Sprintf("@%s", attrName))
	if err != nil {
		return "", err
	}
	return name.String(), nil
}

func FindMessages(wsdl types.Node) types.NodeList {
	return xpath.NodeList(wsdl.Find(xMessages))
}

func FindMessageParts(msg types.Node, msgName string) types.NodeList {
	return xpath.NodeList(msg.Find(fmt.Sprintf(xMessagePartsByMessageName, msgName)))
}

func FindSoapBindingNs(wsdl types.Node, bindingName string) (string, error) {
	soapBindings := xpath.NodeList(wsdl.Find(fmt.Sprintf(xSoapBinding, bindingName)))
	if len(soapBindings) == 0 {
		return "", fmt.Errorf("couldn't locate soap binding for wsdl binding [%s]", bindingName)
	}
	binding := soapBindings[0]
	nodeName := binding.NodeName()
	nsAndName := strings.Split(nodeName, ":")
	if len(nsAndName) == 1 {
		tns := xpath.String(wsdl.Find("@targetNamespace"))
		return tns, nil
	}
	ns := nsAndName[0]
	namespaceURI, err := wsdl.LookupNamespaceURI(ns)
	if err != nil {
		return "", err
	}

	return namespaceURI, nil
}

func FindBindingByName(wsdl types.Node, name string) (types.Node, error) {
	result := xpath.NodeList(wsdl.Find(fmt.Sprintf(xBindingByName, name)))
	if len(result) == 0 {
		return nil, fmt.Errorf("binding with name %s was not found", name)
	}
	return result[0], nil
}

func FindSchemas(wsdl types.Node) types.NodeList {
	return xpath.NodeList(wsdl.Find(xSchemas))
}

func FindPortTypeByName(wsdl types.Node, name string) (types.Node, error) {
	portTypeList := xpath.NodeList(wsdl.Find(fmt.Sprintf(xPortType, name)))
	if len(portTypeList) == 0 {
		return nil, fmt.Errorf("coudln't locate PortType with name %s", name)
	}
	return portTypeList[0], nil
}

func FindServiceByName(wsdl types.Node, name string) (types.Node, error) {
	var result types.NodeList
	if name == "" {
		result = xpath.NodeList(wsdl.Find(xServices))
	} else {
		result = xpath.NodeList(wsdl.Find(fmt.Sprintf(xServiceByName, name)))
	}

	if len(result) == 0 {
		return nil, fmt.Errorf("service with name [%s] was not found", name)
	} else if len(result) > 1 {
		return nil, fmt.Errorf("found more than one service")
	}

	return result[0], nil
}

func FindServicePortBindingByName(service types.Node, serviceName string, portName string) (string, error) {
	var portBindingFound types.NodeList
	if portName != "" {
		portBindingFound = xpath.NodeList(service.Find(fmt.Sprintf(xServicePortBindingByName, serviceName, portName)))
	} else {
		portBindingFound = xpath.NodeList(service.Find(fmt.Sprintf(xServicePortBinding, serviceName)))
	}
	if len(portBindingFound) == 0 {
		return "", fmt.Errorf("service [%s] port was not found", serviceName)
	} else if len(portBindingFound) > 1 {
		return "", fmt.Errorf("found more than one service [%s] port. you must specify port name", serviceName)
	}
	bindingName, err := portBindingFound[0].Literal()
	if err != nil {
		return "", err
	}
	if bindingName == "" {
		return "", fmt.Errorf("couldn't locate service [%s] port binding", serviceName)
	}
	bindingName = common.Stripns(bindingName)

	return bindingName, nil
}

func FindBindingOperationsByName(wsdl types.Node, bindingName string) types.NodeList {
	return xpath.NodeList(wsdl.Find(fmt.Sprintf(xBindingOperationsByName, bindingName)))
}
