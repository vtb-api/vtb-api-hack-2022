package main

import (
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

const headerReturnRs = "return"
const defaultRs = "default.yaml"

type StubResponse struct {
	StatusCode int               `yaml:"status_code"`
	Headers    map[string]string `yaml:"headers"`
	Body       string            `yaml:"body"`
}

var responses map[string]*StubResponse

func main() {
	http.HandleFunc("/", returnMock)
	log.Fatal(http.ListenAndServe("0.0.0.0:9090", nil))
}

func returnMock(w http.ResponseWriter, r *http.Request) {
	rsName := r.Header.Get(headerReturnRs)
	if rsName == "" {
		rsName = defaultRs
	}
	raw, _ := ioutil.ReadAll(r.Body)
	log.Printf("received rq:\n%s\n", string(raw))
	rs := responses[rsName]
	w.WriteHeader(rs.StatusCode)
	for k, v := range rs.Headers {
		w.Header().Set(k, v)
	}
	_, _ = w.Write([]byte(rs.Body))
}

func init() {
	dir, ok := os.LookupEnv("RS_DIR")
	if !ok {
		dir = "responses"
	}
	fileInfos, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatalf("failed to read responses dir: %v", err)
	}
	log.Println("starting file read")
	responses = make(map[string]*StubResponse, len(fileInfos))
	for _, info := range fileInfos {
		raw, err := ioutil.ReadFile(filepath.Join(dir, info.Name()))
		if err != nil {
			log.Fatalf("failed to read file %s", info.Name())
		}
		rs := StubResponse{}
		err = yaml.Unmarshal(raw, &rs)
		if err != nil {
			log.Fatalf("failed to unmarshal file %s", info.Name())
		}
		responses[info.Name()] = &rs
	}
	log.Printf("finished reading mock responses. count %v", len(responses))
}
