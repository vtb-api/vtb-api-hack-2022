package main

import (
	"encoding/json"
	"fmt"
	"github.com/TykTechnologies/tyk/apidef"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"github.com/rshlin/smev_core/soap"
	"github.com/rshlin/smev_core/wsdl"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

const defaultSmevPath = "smev.json"
const defaultBadResponseStatusCode = http.StatusNotImplemented
const defaultLocalCacheTtlSeconds = 300
const defaultValidatorGenerationParallelism = 5
const defaultWsdlDir = "wsdl"
const pluginConfKey = "smev"

var pluginConfig *PluginConfig

type File struct {
	Name    string `mapstructure:"name" json:"name"`
	Content string `mapstructure:"content" json:"content"`
}
type ApiConfig struct {
	RawWsdl     *File         `mapstructure:"wsdl" json:"wsdl"`
	Schemas     []*File       `mapstructure:"schemas" json:"schemas"`
	WsdlOptions *wsdl.Options `mapstructure:"wsdl_options" json:"wsdl_options"`
	SoapOptions *soap.Options `mapstructure:"soap_options" json:"soap_options"`
}

type PluginConfig struct {
	BadResponseStatusCode         int    `mapstructure:"bad_response_status_code" json:"bad_response_status_code"`
	LocalCacheTtlSeconds          int64  `mapstructure:"local_cache_ttl_seconds" json:"local_cache_ttl_seconds"`
	ValidatorProvisionParallelism int    `mapstructure:"validator_provision_parallelism" json:"validator_provision_parallelism"`
	WsdlDir                       string `mapstructure:"wsdl_dir" json:"wsdl_dir"`
}

func GetApiConfig(apidef *apidef.APIDefinition) (*ApiConfig, error) {
	var apiConfig *ApiConfig
	configData := apidef.ConfigData
	apiConfigRaw, ok := configData[pluginConfKey]
	if !ok {
		return nil, fmt.Errorf("couldn't locate smev api config from apiId [%s]", apidef.APIID)
	}
	err := mapstructure.Decode(apiConfigRaw, &apiConfig)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to decode api config from apiId [%s]", apidef.APIID)
	}
	if apiConfig.RawWsdl == nil {
		return nil, errors.Wrapf(err, "failed to decode wsdl from apiId [%s]", apidef.APIID)
	}
	if apiConfig.WsdlOptions == nil {
		return nil, errors.Wrapf(err, "failed to decode wsdl options from apiId [%s]", apidef.APIID)
	}
	if apiConfig.SoapOptions == nil {
		return nil, errors.Wrapf(err, "failed to decode soap options from apiId [%s]", apidef.APIID)
	}
	return apiConfig, nil
}

func GetPluginConfig() *PluginConfig {
	return pluginConfig
}

func fetchPluginConfig() *PluginConfig {
	path := defaultSmevPath
	defaultConf := func() *PluginConfig {
		return &PluginConfig{
			BadResponseStatusCode:         defaultBadResponseStatusCode,
			LocalCacheTtlSeconds:          defaultLocalCacheTtlSeconds,
			ValidatorProvisionParallelism: defaultValidatorGenerationParallelism,
			WsdlDir:                       defaultWsdlDir,
		}
	}
	f, err := os.Open(path)
	if err != nil {
		logger.Errorf("failed to open [%s]: %v", path, err)
		return defaultConf()
	}
	defer func(f *os.File) {
		_ = f.Close()
	}(f)

	var conf map[string]interface{}
	if err = json.NewDecoder(f).Decode(&conf); err != nil {
		logger.Errorf("failed to decode [%s]: %v", path, err)
		return defaultConf()
	}
	pluginConf := conf

	var typedPluginConfig PluginConfig
	err = mapstructure.Decode(pluginConf, &typedPluginConfig)
	if err != nil {
		logger.Errorf("couldn't decode smev plugin config: %v", err)
		return defaultConf()
	}

	return &typedPluginConfig
}

func init() {
	pluginConfig = fetchPluginConfig()
	createWsdlDir(pluginConfig)
}

func createWsdlDir(pluginConfig *PluginConfig) {
	if pluginConfig.WsdlDir == "" {
		pluginConfig.WsdlDir = defaultWsdlDir
	}
	if !filepath.IsAbs(pluginConfig.WsdlDir) {
		wd, err := os.Getwd()
		if err != nil {
			logger.Fatalf("failed to get working dir: %v", err)
		}
		pluginConfig.WsdlDir = filepath.Join(wd, pluginConfig.WsdlDir)
	}
	logger.Infof("wsdl abs dir path: [%s]", pluginConfig.WsdlDir)
	_ = os.Mkdir(pluginConfig.WsdlDir, 0644)
	if err := removeContents(pluginConfig.WsdlDir); err != nil {
		log.Fatalf("failed to cleanup wsdl dir: %v", err)
	}
}

func removeContents(dir string) error {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer func(d *os.File) {
		_ = d.Close()
	}(d)
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}
	return nil
}
