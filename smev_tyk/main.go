package main

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/TykTechnologies/tyk-pump/analytics"
	"github.com/TykTechnologies/tyk/headers"
	"github.com/TykTechnologies/tyk/log"
	"github.com/google/uuid"
	"github.com/lestrrat-go/libxml2"
	"github.com/lestrrat-go/libxml2/xsd"
	"github.com/pkg/errors"
	"github.com/rshlin/smev_core/soap"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/TykTechnologies/tyk/ctx"
	//"github.com/TykTechnologies/tyk/headers"
	//"github.com/TykTechnologies/tyk/log"
	//"github.com/TykTechnologies/tyk/user"
)

const headerSoapAction = "SOAPAction"
const headerXSoapAction = "X-SOAPAction"
const tagSoapAction = "SOAPAction"

var logger = log.Get()

func init() {
	logger.Info("Initialising SMEV Tyk Plugin")
	pluginConfig := GetPluginConfig()
	rawCfg, _ := json.MarshalIndent(pluginConfig, "", "  ")
	logger.Infof("SMEV Tyk Plugin configuration:\n%v", string(rawCfg))
}

func ValidateSoapRequest(rw http.ResponseWriter, rq *http.Request) {
	rqId, err := uuid.NewUUID()
	if err != nil {
		handleError(rw, http.StatusInternalServerError, err)
		return
	}
	logger.Debugf("processing request [%s]", rqId.String())

	soapActionHeader := rq.Header.Get(headerSoapAction)
	if soapActionHeader == "" {
		handleError(rw, http.StatusBadRequest, fmt.Errorf("SOAPAction header was not found"))
		return
	}
	if !hasXMLContentType(rq.Header) {
		handleError(rw, http.StatusUnsupportedMediaType, fmt.Errorf("only %s : %s is supported", headers.ContentType, headers.TextXML))
		return
	}
	logger.Debugf("soap action: %s", soapActionHeader)
	soapAction := soap.Action(soapActionHeader)
	apiDefinition := ctx.GetDefinition(rq)
	if apiDefinition == nil {
		handleError(rw, http.StatusInternalServerError, fmt.Errorf("couldn't locate api definition"))
		return
	}
	config, err := GetApiConfig(apiDefinition)
	if err != nil {
		handleError(rw, http.StatusInternalServerError, errors.Wrapf(err, "couldn't locate api custom config"))
		return
	}
	setApiConfig(rq, config)
	body, err := ioutil.ReadAll(rq.Body)
	if err != nil {
		handleError(rw, http.StatusInternalServerError, err)
		return
	}
	doc, err := libxml2.Parse(body)
	if err != nil {
		handleError(rw, http.StatusBadRequest, err)
		return
	}
	logger.Debugf("requesting validator for rq [%s]", rqId.String())
	validator, err := GetValidator(apiDefinition, config)
	if err != nil {
		handleError(rw, http.StatusInternalServerError, err)
		return
	}
	logger.Debugf("validating body of rq [%s]", rqId.String())
	err = validator.ValidateRq(doc, soapAction)
	if err != nil {
		handleError(rw, http.StatusBadRequest, err)
		return
	}

	setRqId(rq, &rqId)
	setSoapAction(rq, &soapAction)
	rq.Body = ioutil.NopCloser(bytes.NewBuffer(body))

	logger.Debugf("finished processing request [%s]", rqId.String())
}

func ValidateSoapResponse(rw http.ResponseWriter, rs *http.Response, rq *http.Request) {
	rqId := getRqId(rq)
	if rqId == nil {
		handleError(rw, http.StatusInternalServerError, fmt.Errorf("couldn't locate request id. the middleware is activated without request validator mw"))
		return
	}
	logger.Debugf("processing response [%s]", rqId.String())

	if !hasXMLContentType(rs.Header) {
		handleError(rw, GetPluginConfig().BadResponseStatusCode, fmt.Errorf("only %s : %s is supported", headers.ContentType, headers.TextXML))
		return
	}
	soapAction := getSoapAction(rq)
	if soapAction == nil {
		handleError(rw, http.StatusInternalServerError, fmt.Errorf("couldn't locate soap action. the middleware is activated without request validator mw"))
		return
	}
	logger.Debugf("soap action [%v]", soapAction)
	apiDefinition := ctx.GetDefinition(rq)
	if apiDefinition == nil {
		handleError(rw, http.StatusInternalServerError, fmt.Errorf("couldn't locate api definition"))
		return
	}
	config := getApiConfig(rq)
	if config == nil {
		handleError(rw, http.StatusInternalServerError, fmt.Errorf("couldn't locate api config. the middleware is activated without request validator mw"))
		return
	}

	body, err := ioutil.ReadAll(rs.Body)
	if err != nil {
		handleError(rw, http.StatusInternalServerError, err)
		return
	}
	doc, err := libxml2.Parse(body)
	if err != nil {
		handleError(rw, GetPluginConfig().BadResponseStatusCode, err)
		return
	}
	validator, err := GetValidator(apiDefinition, config)
	if err != nil {
		handleError(rw, http.StatusInternalServerError, err)
		return
	}
	err = validator.ValidateRs(doc, *soapAction)
	if err != nil {
		handleError(rw, GetPluginConfig().BadResponseStatusCode, err)
		return
	}

	rs.Body = ioutil.NopCloser(bytes.NewBuffer(body))
	rs.Header.Add(headerXSoapAction, string(*soapAction))

	logger.Debugf("finished processing response [%s]", rqId.String())
}

func hasXMLContentType(header http.Header) bool {
	containsTextXml := false
	h := header.Get(headers.ContentType)
	ct := strings.Split(h, ";")
	for _, headerValue := range ct {
		headerValue = strings.TrimSpace(headerValue)
		if headerValue == headers.TextXML {
			containsTextXml = true
			break
		}
	}
	return containsTextXml
}

func handleError(rw http.ResponseWriter, statusCode int, err error) {
	rw.WriteHeader(statusCode)
	rw.Header().Add(headers.ContentType, headers.TextXML)
	data := getSoapFault(statusCode, err)
	_, err = rw.Write(data.Bytes())
	if err != nil {
		logger.Errorf("failed to write response body")
	}
}

func getSoapFault(statusCode int, err error) *bytes.Buffer {
	data := new(bytes.Buffer)

	messages := []string{errors.WithStack(err).Error()}
	if sve, ok := err.(xsd.SchemaValidationError); ok {
		messages = append(messages, "============Validation Errors================")
		for _, e := range sve.Errors() {
			messages = append(messages, e.Error())
		}
		messages = append(messages, "=============================================")
	}

	templateData := &soap.FaultTemplateData{
		Code:    statusCode,
		Message: strings.Join(messages, "\n"),
	}
	_ = soap.FaultTmpl.Execute(data, templateData)
	return data
}

func RecordSoapEvent(record *analytics.AnalyticsRecord) {
	logger.Debugf("processing soap record for apiid [%s]", record.APIID)
	str, err := base64.StdEncoding.DecodeString(record.RawRequest)
	if err != nil {
		return
	}

	var b = &bytes.Buffer{}
	b.Write(str)

	r := bufio.NewReader(b)
	var rq *http.Request
	rq, err = http.ReadRequest(r)
	if err != nil {
		return
	}

	soapAction := rq.Header.Get(headerSoapAction)
	if soapAction == "" {
		logger.Errorf("failed to locate request header [%s]. the middleware is activated without request mw", headerSoapAction)
	} else {
		//TODO Verify
		record.Path = soapAction
		record.RawPath = soapAction
		record.TrackPath = true
		record.Tags = append(record.Tags, fmt.Sprintf("%s-%s", tagSoapAction, soapAction))
	}

	var bNew bytes.Buffer
	_ = rq.Write(&bNew)
	record.RawResponse = base64.StdEncoding.EncodeToString(bNew.Bytes())
	logger.Debugf("finished processing soap record for apiid [%s]", record.APIID)
}

func main() {}
