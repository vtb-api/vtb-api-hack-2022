package main

import (
	"github.com/pkg/errors"
	"github.com/rshlin/smev_core/soap"
	"github.com/rshlin/smev_core/wsdl"
	"io/ioutil"
	"os"
	"path/filepath"
)

var generateValidatorChan chan GenerateValidatorRequest

type GenerateValidatorRequest struct {
	apiKey  string
	apiInfo apiInfo
	rsChan  chan GenerateValidatorResponse
}

type GenerateValidatorResponse struct {
	apiKey string
	result interface{}
}

func init() {
	generateValidatorChan = make(chan GenerateValidatorRequest)
	for i := 0; i < GetPluginConfig().ValidatorProvisionParallelism; i++ {
		go handleGenerateValidatorRequest(generateValidatorChan)
	}
}

func handleGenerateValidatorRequest(generateValidatorChan chan GenerateValidatorRequest) {
	for {
		select {
		case rq := <-generateValidatorChan:
			handleRq(rq)
		}
	}
}

func handleRq(rq GenerateValidatorRequest) {
	logger.Infof("generating validator for apiId [%s]", rq.apiInfo.ApiId)
	defer func() {
		if err := recover(); err != nil {
			logger.Errorf("unexpected panic occured for apiId [%s]: %v", rq.apiInfo.ApiId, err)
			handleGenError(rq, err.(error))
		}
	}()
	dir := GetPluginConfig().WsdlDir
	wsdlDir := filepath.Join(dir, rq.apiInfo.ApiId)
	err := os.Mkdir(wsdlDir, 0777)
	if err != nil {
		handleGenError(rq, err)
		return
	}
	defer func(path string) {
		err := os.RemoveAll(path)
		if err != nil {

		}
	}(wsdlDir)

	if err := extractFiles(rq.apiInfo, wsdlDir); err != nil {
		handleGenError(rq, err)
		return
	}

	logger.Debugf("creating wsdl for apiId [%s]", rq.apiInfo.ApiId)
	wsdlPath := filepath.Join(wsdlDir, rq.apiInfo.ApiConfig.RawWsdl.Name)
	w, err := wsdl.NewWsdlFromFile(wsdlPath, rq.apiInfo.ApiConfig.WsdlOptions)
	if err != nil {
		handleGenError(rq, err)
		return
	}
	err = w.FlushRootSchemas()
	if err != nil {
		handleGenError(rq, err)
		return
	}
	logger.Debugf("creating validator for apiId [%s]", rq.apiInfo.ApiId)
	validator, err := soap.NewSoapValidator(w, rq.apiInfo.ApiConfig.SoapOptions)
	if err != nil {
		handleGenError(rq, err)
		return
	}

	result := GenerateValidatorResponse{
		apiKey: rq.apiKey,
		result: validator,
	}
	logger.Infof("finished generating validator for apiId [%s]", rq.apiInfo.ApiId)
	rq.rsChan <- result
}

func extractFiles(info apiInfo, wsdlDir string) error {
	files := append(info.ApiConfig.Schemas, info.ApiConfig.RawWsdl)
	for _, file := range files {
		if err := extractFile(file, wsdlDir); err != nil {
			return err
		}
	}
	return nil
}

func extractFile(file *File, wsdlDir string) error {
	filePath := filepath.Join(wsdlDir, file.Name)
	if err := ioutil.WriteFile(filePath, []byte(file.Content), 0666); err != nil {
		return errors.Wrapf(err, "failed to persist file [%s]", filePath)
	}
	return nil
}

func handleGenError(rq GenerateValidatorRequest, err error) {
	result := GenerateValidatorResponse{
		apiKey: rq.apiKey,
		result: err,
	}
	rq.rsChan <- result
}
