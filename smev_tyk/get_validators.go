package main

import (
	"github.com/TykTechnologies/tyk/apidef"
	"github.com/mitchellh/hashstructure/v2"
	"github.com/rshlin/smev_core/soap"
	"strconv"
	"time"
)

var validatorsByApiKey map[string]soap.Validator
var pendingRequestsByApiId map[string][]chan interface{}
var validatorLastAccess map[string]int64

var getValidatorChan chan GetValidatorRequest
var processGenResult chan GenerateValidatorResponse

var flushByTTl chan interface{}

func init() {
	validatorsByApiKey = make(map[string]soap.Validator)
	pendingRequestsByApiId = make(map[string][]chan interface{})
	validatorLastAccess = make(map[string]int64)
	getValidatorChan = make(chan GetValidatorRequest)
	processGenResult = make(chan GenerateValidatorResponse)

	go handleRequests(generateValidatorChan, flushByTTl)
	go scheduleFlush(GetPluginConfig().LocalCacheTtlSeconds, flushByTTl)
}

type apiInfo struct {
	ApiId     string
	ApiConfig ApiConfig
}

type GetValidatorRequest struct {
	apiInfo apiInfo
	rsChan  chan interface{}
}

func handleRequests(generateValidatorChan chan GenerateValidatorRequest, flushByTTl chan interface{}) {
	logger.Infof("started handler of validator requests")
	for {
		select {
		case rq := <-getValidatorChan:
			handleGetRequest(generateValidatorChan, rq)
		case rs := <-processGenResult:
			handleGenerateResponse(rs)
		case _ = <-flushByTTl:
			handleFlush()
		}
	}
}

func handleGetRequest(generateValidatorChan chan GenerateValidatorRequest, rq GetValidatorRequest) {
	apiKey := rq.apiInfo.Hash()
	logger.Debugf("api with id [%s] has apiKey [%s]", rq.apiInfo.ApiId, apiKey)
	if v, ok := validatorsByApiKey[apiKey]; ok {
		logger.Debugf("found existing validator for apiKey [%s]", apiKey)
		recordAccess(apiKey)
		rq.rsChan <- v
		return
	}
	logger.Debugf("couldn't find existing validator for apiKey [%s]. requesting generation", apiKey)
	pr, ok := pendingRequestsByApiId[apiKey]
	if !ok {
		pr = []chan interface{}{rq.rsChan}
		genRq := GenerateValidatorRequest{
			apiInfo: rq.apiInfo,
			apiKey:  apiKey,
			rsChan:  processGenResult,
		}
		generateValidatorChan <- genRq
	} else {
		pr = append(pr, rq.rsChan)
	}
	pendingRequestsByApiId[apiKey] = pr
}

func recordAccess(apiKey string) {
	now := time.Now().Unix()
	validatorLastAccess[apiKey] = now
}

func handleGenerateResponse(rs GenerateValidatorResponse) {
	logger.Debugf("received validator gen response for apiKey [%s]", rs.apiKey)
	if v, ok := rs.result.(soap.Validator); ok {
		logger.Debugf("validator was successfully created for apiKey [%s]", rs.apiKey)
		validatorsByApiKey[rs.apiKey] = v
		recordAccess(rs.apiKey)
	} else {
		logger.Errorf("received error result for apiKey [%s]: %v", rs.apiKey, rs.result)
	}
	if rq, ok := pendingRequestsByApiId[rs.apiKey]; ok {
		for _, c := range rq {
			c <- rs.result
		}
		delete(pendingRequestsByApiId, rs.apiKey)
	}
}

func scheduleFlush(localCacheTtlSeconds int64, flushByTtl chan interface{}) {
	for {
		time.Sleep(time.Duration(localCacheTtlSeconds) * time.Second)
		logger.Debugf("triggering flusuh by ttl")
		flushByTtl <- struct{}{}
	}
}

func handleFlush() {
	threshold := time.Now().Unix() - GetPluginConfig().LocalCacheTtlSeconds
	var toFlush []string
	for apiKey, lastAccess := range validatorLastAccess {
		if lastAccess < threshold {
			toFlush = append(toFlush, apiKey)
		}
	}
	logger.Info("flushing validators. api keys: %v", toFlush)
	for _, apiKey := range toFlush {
		delete(validatorLastAccess, apiKey)
		delete(validatorsByApiKey, apiKey)
	}
}

func GetValidator(apidef *apidef.APIDefinition, apiConfig *ApiConfig) (soap.Validator, error) {
	apiKey := getApiKey(apidef, apiConfig)
	rsChan := make(chan interface{}, 1)
	defer close(rsChan)

	request := GetValidatorRequest{
		apiInfo: apiKey,
		rsChan:  rsChan,
	}
	getValidatorChan <- request

	result := <-rsChan

	v, ok := result.(soap.Validator)
	if !ok {
		return nil, result.(error)
	}

	return v, nil
}

func getApiKey(apidef *apidef.APIDefinition, apiConfig *ApiConfig) apiInfo {
	return apiInfo{
		ApiId:     apidef.APIID,
		ApiConfig: *apiConfig,
	}
}

func (a *apiInfo) Hash() string {
	hash, _ := hashstructure.Hash(a, hashstructure.FormatV2, nil)
	return strconv.FormatUint(hash, 10)
}
