module github.com/rshlin/smev_tyk

go 1.15

//replace github.com/jensneuse/graphql-go-tools => github.com/TykTechnologies/graphql-go-tools v1.6.2-0.20220426094453-0cc35471c1ca

require github.com/TykTechnologies/tyk v1.9.2-0.20220207163550-272b8ba0fcb3

require github.com/TykTechnologies/tyk-pump v1.6.0

require (
	github.com/google/uuid v1.1.2
	github.com/lestrrat-go/libxml2 v0.0.0-20201123224832-e6d9de61b80d
	github.com/mitchellh/hashstructure/v2 v2.0.2
	github.com/mitchellh/mapstructure v1.5.0
	github.com/pkg/errors v0.9.1
	github.com/rshlin/smev_core v1.2.3
)

replace github.com/rshlin/smev_core => ./../smev_core
