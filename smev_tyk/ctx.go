package main

import (
	"context"
	"github.com/google/uuid"
	"github.com/rshlin/smev_core/soap"
	"net/http"
)

const ctxSoapAction = "ctxSOAPAction"
const ctxRqId = "ctxRqId"
const ctxApiConfig = "ctxApiConfig"

func getApiConfig(r *http.Request) *ApiConfig {
	if v := r.Context().Value(ctxApiConfig); v != nil {
		return v.(*ApiConfig)
	}
	return nil
}

func setApiConfig(r *http.Request, apiConfig *ApiConfig) {
	reqCtx := r.Context()
	reqCtx = context.WithValue(reqCtx, ctxApiConfig, apiConfig)
	setContext(r, reqCtx)
}

func getRqId(r *http.Request) *uuid.UUID {
	if v := r.Context().Value(ctxRqId); v != nil {
		return v.(*uuid.UUID)
	}
	return nil
}

func setRqId(r *http.Request, id *uuid.UUID) {
	reqCtx := r.Context()
	reqCtx = context.WithValue(reqCtx, ctxRqId, id)
	setContext(r, reqCtx)
}

func getSoapAction(r *http.Request) *soap.Action {
	if v := r.Context().Value(ctxSoapAction); v != nil {
		return v.(*soap.Action)
	}
	return nil
}

func setSoapAction(r *http.Request, action *soap.Action) {
	reqCtx := r.Context()
	reqCtx = context.WithValue(reqCtx, ctxSoapAction, action)
	setContext(r, reqCtx)
}

func setContext(r *http.Request, ctx context.Context) {
	r2 := r.WithContext(ctx)
	*r = *r2
}
